#ifndef GLUTIL_H
#define GLUTIL_H
#include <GL/glew.h>
#include <GL/glut.h>

#ifdef __cplusplus
extern "C" {
#endif

GLuint loadShaders(const char* vertexSource, const char* fragmentSource);

#ifdef __cplusplus
}
#endif

#endif // GLUTIL_H
