#ifndef READ_MAT_H
#define READ_MAT_H
    void readArray(float** arrayPtr, int* dims, const char* file);
    void freeArray(float* arr);
#endif
