/*
 * Reads a MAT file and dumps the contents to console
 *
 * Usage:
 *
 *    read_mat <matfile>
 */
#include <stdio.h>
#include <stdlib.h>

// Matlab
#include "mat.h"
#include "matrix.h"

// readArray reads a MAT file into an array, and returns a pointer to the
// array corresponding to the matrix.
void readArray(float** arrayPtr, int* dims, const char* file)
{
    MATFile* mat; // reference to the .mat file
    const char* name = NULL; // name of matrix
    mxArray* matlabArr; // the array

    // Open file
    mat = matOpen(file, "r");
    if (mat == NULL) {
        printf("Error opening file %s\n", file);
        exit(1);
    }

    // Read the points into a col major array, and retrieve the number of
    // dimensions.
    matlabArr = matGetNextVariable(mat, &name);

    if (matlabArr == NULL) {
        printf("Error reading in file %s, matrix %s\n", file, name);
        exit(1);
    }
    dims[0] = mxGetN(matlabArr);
    dims[1] = mxGetM(matlabArr);

    // Close file
    if (matClose(mat) != 0) {
        printf("Error closing file %s\n", file);
        exit(1);
    }

    // Copy double points to a newly allocated row major array of floats which
    // is easier for OpenGL to use
    double* colMajorArray = (double*)mxGetPr(matlabArr);
    float* array = (float*)malloc(sizeof(float)*dims[0]*dims[1]);
    for (int n = 0; n < dims[1]; n++) {
        for (int m = 0; m < dims[0]; m++) {
            array[n*dims[0] + m] = (float)colMajorArray[m*dims[1] + n];
        }
    }

    // Free the matlab array
    mxDestroyArray(matlabArr);

    // Return address to float array
    *arrayPtr = array;
}

// freeArray frees the allocated memory associated with an array created using
// readArray.
void freeArray(float* arr) {
    free(arr);
}
