#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glew.h>
#include <GL/glut.h>

#include "glutil.h"

// loadShaders compiles a program consisting of a vertex shader and a fragment
// shader, and returns the ID of the program.
// Source: http://www.opengl-tutorial.org/
GLuint loadShaders(const char* vertexSource, const char* fragmentSource)
{
    // Create the shaders
    GLuint vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);

    GLint result = GL_FALSE;
    int infoLogLength;

    // Compile vertex shader
    glShaderSource(vertexShaderId, 1, &vertexSource, NULL);
    glCompileShader(vertexShaderId);

    // Check vertex shader
    glGetShaderiv(vertexShaderId, GL_COMPILE_STATUS, &result);
    if (!result) {
        glGetShaderiv(vertexShaderId, GL_INFO_LOG_LENGTH, &infoLogLength);
        char* vertexShaderErrorMessage = (char*)malloc(sizeof(char) * infoLogLength + 1);
        glGetShaderInfoLog(vertexShaderId, infoLogLength, NULL, vertexShaderErrorMessage);
        printf("Vertex shader compile error:\n%s\n", vertexShaderErrorMessage);
        free(vertexShaderErrorMessage);
    }

    // Compile fragment shader
    glShaderSource(fragmentShaderId, 1, &fragmentSource, NULL);
    glCompileShader(fragmentShaderId);

    // Check fragment shader
    glGetShaderiv(fragmentShaderId, GL_COMPILE_STATUS, &result);
    if (!result) {
        glGetShaderiv(fragmentShaderId, GL_INFO_LOG_LENGTH, &infoLogLength);
        char* fragmentShaderErrorMessage = (char*)malloc(sizeof(char) * infoLogLength + 1);
        glGetShaderInfoLog(fragmentShaderId, infoLogLength, NULL, fragmentShaderErrorMessage);
        printf("Fragment shader compile error:\n%s\n", fragmentShaderErrorMessage);
        free(fragmentShaderErrorMessage);
    }

    // Link the program
    GLuint programId = glCreateProgram();
    glAttachShader(programId, vertexShaderId);
    glAttachShader(programId, fragmentShaderId);
    glLinkProgram(programId);

    // Check the program
    glGetProgramiv(programId, GL_LINK_STATUS, &result);
    if (!result) {
        glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &infoLogLength);
        char* programErrorMessage = (char*)malloc(sizeof(char) * infoLogLength + 1);
        glGetProgramInfoLog(programId, infoLogLength, NULL, programErrorMessage);
        printf("Program link error:\n%s\n", programErrorMessage);
    }

    // Cleanup shaders
    glDeleteShader(vertexShaderId);
    glDeleteShader(fragmentShaderId);

    return programId;
}
