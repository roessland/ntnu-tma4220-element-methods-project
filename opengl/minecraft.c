#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

// Read matlab matrix
#include "read_mat.h" 

// OpenGL
#include "GL/glew.h"
#include <GL/glut.h>
#include "glutil.h"
#include "linmath.h"

// Some simple max/min macros
#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })
#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

#define PI 3.14159265359f

void glCheckError(char* msg) {
    GLenum err = glGetError();
    if (err != GL_NO_ERROR) {
    switch (err) {
        case GL_INVALID_ENUM:
            printf("%s OpenGL error: A function has been called with an invalid enum\n", msg);
            break;
        case GL_INVALID_VALUE:
            printf("%s OpenGL error: Invalid value\n", msg);
            break;
        case GL_INVALID_OPERATION:
            printf("%s OpenGL error: Invalid operation\n", msg);
            break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            printf("%s OpenGL error: Invalid framebuffer operation\n", msg);
            break;
        case GL_OUT_OF_MEMORY:
            printf("%s OpenGL error: Out of memory\n", msg);
            break;
        default:
            printf("%s: Unknown GL error\n", msg);
            break;
    }
    exit(1);
    }
}

// Globals
float time = 0.0;
int displayMode = 1; // 1 = show colors, 2 = show stress

// Data
float* pts;
float* tetr;
float* displacement;
float* stress;
int pts_dims[2];
int tetr_dims[2];
int displacement_dims[2];
int stress_dims[2];

// Processed data
float* vertexPos;
float* vertexNormal;
float* vertexDisplacement;
float* vertexColor;
int* materialType;
float* vertexStress;

// OpenGL handles
GLuint programId;
GLuint vertexPosBuffer; // 3 floats per vertex
GLuint vertexNormalBuffer; // 3 floats per vertex
GLuint vertexDisplacementBuffer; // 3 floats per vertex
GLuint vertexColorBuffer;
GLuint vertexStressBuffer;
GLuint uTime;
GLuint uMVP;
GLuint uEyePos;
GLuint uSunPos;
GLuint uDisplayMode;

// Projection
mat4x4 MVP;
mat4x4 model;
mat4x4 view;
mat4x4 perspective;
vec3 eyePos;
vec3 centerPos = {0.0f, 0.0f, 0.0f};
vec3 upDir = {0.0f, 1.0f, 0.0f};
vec3 sunPos = {1000.0f, 11000.0f, 0.0f};

// Vertex shader transforms vertex positions
char* simpleVertexShader =
"#version 330 core\n"
"\n"
"layout(location = 0) in vec3 vertexPosition_modelspace;\n"
"layout(location = 1) in vec3 vertexNormal_modelspace;\n"
"layout(location = 2) in vec3 vertexDisplacement_modelspace;\n"
"layout(location = 3) in vec3 vertexColor;\n"
"layout(location = 4) in float vertexStress;\n"
"\n"
"uniform float uTime;\n"
"uniform mat4 uMVP;\n"
"uniform vec3 uEyePos;\n"
"uniform vec3 uSunPos;\n"
"uniform int uDisplayMode;\n"
"\n"
"smooth out vec3 fragPosition_modelspace;\n"
"smooth out float lightness;\n"
"smooth out vec3 color;\n"
"\n"
"vec3 getJetColor(float value) {\n"
"   float fourValue = 4 * value;\n"
"   float red = min(fourValue - 1.5, -fourValue + 4.5);\n"
"   float green = min(fourValue - 0.5, -fourValue + 3.5);\n"
"   float blue = min(fourValue + 0.5, -fourValue + 2.5);\n"
"   return clamp(vec3(red, green, blue), 0.0, 1.0);\n"
"}\n"
"\n"
"void main() {\n"
"   gl_Position = uMVP * vec4(vertexPosition_modelspace + vertexDisplacement_modelspace*(1+sin(0.2*uTime))*4, 1);\n"
"   fragPosition_modelspace = vertexPosition_modelspace;\n"
"\n"
"   vec3 dirToLight = normalize(uSunPos - vertexPosition_modelspace);\n"
"   float diffuse = clamp(dot(dirToLight, normalize(vertexNormal_modelspace)), 0, 1);\n"
"   lightness = 0.5 * diffuse + 0.5;\n"
"   if (uDisplayMode == 1) {\n"
"       color = vertexColor;\n"
"   } else {\n"
"       color = getJetColor(vertexStress*30);\n"
"   }\n"
"}\n";

// Fragment shader adds color to triangles
char* simpleFragmentShader =
"#version 330 core\n"
"uniform int uDisplayMode;\n"
"out vec3 gl_FragColor;\n"
"smooth in float lightness;\n"
"smooth in vec3 color;\n"
"\n"
"void main()\n"
"{\n"
"   if (uDisplayMode == 1) {\n"
"       gl_FragColor = color*lightness;\n"
"   } else {\n"
"       gl_FragColor = clamp(color, 0.1, 0.9);\n"
"   }\n"
"}\n";


void generateMVPMatrix(void)
{
    // Update projection matrix: 
    // 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    mat4x4_perspective(perspective, PI/2, 1.6f, 1.9f, 70.0f);

    // Update view matrix
    //centerPos[0] = 36.25f;
    //centerPos[1] = 80.25f; // desert house
    //centerPos[2] = 204.5f;
    //centerPos[0] = 311.0f;
    //centerPos[1] = 47.0f; // building
    //centerPos[2] = 537.0f;
    //centerPos[0] = -512;
    //centerPos[1] = 76; // crane
    //centerPos[2] = 20;
    //centerPos[0] = -153.0f; 
    //centerPos[1] = 94.0f; // bank
    //centerPos[2] = 3.1f;
    centerPos[0] = 102.0f;
    centerPos[1] = 37.0f;
    centerPos[2] = 448.0f;

    eyePos[0] = centerPos[0] + 46.1*cosf(0.05*time);
    eyePos[1] = centerPos[1] + 11;
    eyePos[2] = centerPos[2] + 46.1*sinf(0.05*time);

    upDir[0] = 0.0f;
    upDir[1] = 1.0f;
    upDir[2] = 0.0f;

    mat4x4_look_at(view, eyePos, centerPos, upDir);

    // Update model matrix
    // Model will be unscaled, and positioned at the origin
    mat4x4_identity(model);

    // Update ModelViewPerspective matrix = projection * view * model
    mat4x4_mul(MVP, perspective, view); // MVP = perspective * view
    mat4x4_mul(MVP, MVP, model);        // MVP = perspective * view * model
}

void displayLoop(void)
{
    time += 0.1;

    // Move viewpoint
    generateMVPMatrix();

    // Clear screen and GL buffers
    glClearColor(0.1f, 0.1f, 0.1f, 0.0f); // dark grey for movies
    //glClearColor(1.0f, 1.0f, 1.0f, 0.0f); // white for screenshots
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Use our shaders
    glUseProgram(programId);

    // Upload uniforms
    glUniformMatrix4fv(uMVP, 1, GL_FALSE, &MVP[0][0]);
    glUniform1f(uTime, time);
    glUniform3fv(uEyePos, 1, eyePos);
    glUniform3fv(uSunPos, 1, sunPos);
    glUniform1i(uDisplayMode, displayMode);

    // Bind position and normal attributes and draw
    glEnableVertexAttribArray(0); // vertexPos
    glEnableVertexAttribArray(1); // vertexNormal
    glEnableVertexAttribArray(2); // vertexDisplacement
    glEnableVertexAttribArray(3); // vertexColor
    glEnableVertexAttribArray(4); // vertexStress
    glDrawArrays(GL_TRIANGLES, 0, 36 * tetr_dims[1]);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);
    glDisableVertexAttribArray(4);

    glutSwapBuffers();
    glutPostRedisplay();

}

void keyboardInput(unsigned char key, int x, int y) {
    if (displayMode == 1) {
        displayMode = 2;
    } else {
        displayMode = 1;
    }
}

void initGL(int* argcp, char** argv) 
{
    glutInit(argcp, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glDepthFunc(GL_LESS);
    glutInitWindowSize(1920*1.3, 1200*1.3);
    glutCreateWindow("OpenGL Minecraft");
    glutDisplayFunc(displayLoop);
    glutKeyboardFunc(keyboardInput);
    glewInit();

    programId = loadShaders(simpleVertexShader, simpleFragmentShader);
    glCheckError("get uMVP uniform location");

    uMVP = glGetUniformLocation(programId, "uMVP");
    uEyePos = glGetUniformLocation(programId, "uEyePos");
    uSunPos = glGetUniformLocation(programId, "uSunPos");
    uTime = glGetUniformLocation(programId, "uTime");
    uDisplayMode = glGetUniformLocation(programId, "uDisplayMode");

    glEnable(GL_DEPTH_TEST);
    glCheckError("enable depth test");
}


// getMaterialColor takes a material number and writes the color of the
// material to a given array of floats.
void getMaterialColor(float* color, unsigned int material) {
    float c[3];
    switch (material) {
        case 0:  
        case 1:  c[0] = 0.8f; c[1] = 0.8f; c[2] = 0.8f; break; // stone
        case 2:  c[0] = 0.8f; c[1] = 0.8f; c[2] = 0.8f; break; // stone
        case 3:  c[0] = 0.8f; c[1] = 0.0f; c[2] = 0.8f; break; // dunno
        case 4:  c[0] = 0.8f; c[1] = 0.0f; c[2] = 0.0f; break; // stone?
        case 5:  c[0] = 0.5f; c[1] = 0.5f; c[2] = 0.5f; break; // stone
        case 6:  c[0] = 0.8f; c[1] = 0.3f; c[2] = 0.0f; break; // wood
        case 13:  c[0] = 0.93f; c[1] = 0.84f; c[2] = 0.69f; break; // sand
        case 17:  c[0] = 1.0f; c[1] = 0.0f; c[2] = 1.0f; break; // Dunno
        case 18:  c[0] = 1.0f; c[1] = 0.0f; c[2] = 1.0f; break; // Dunno
        case 19:  c[0] = 0.55f; c[1] = 0.55f; c[2] = 0.0f; break; // Tree tops
        case 20:  c[0] = 1.0f; c[1] = 1.0f; c[2] = 1.0f; break; // Window
        case 21:  c[0] = 1.0f; c[1] = 1.0f; c[2] = 1.0f; break; // Glass windows
        case 23:  c[0] = 0.0f; c[1] = 0.0f; c[2] = 1.0f; break; // Lapis Lazuli
        case 25:  c[0] = 0.0f; c[1] = 0.0f; c[2] = 0.0f; break; // Dunno
        case 26:  c[0] = 0.0f; c[1] = 0.0f; c[2] = 1.0f; break; // Dunno
        case 33:  c[0] = 0.4f; c[1] = 0.7f; c[2] = 0.0f; break; // Grass/weed
        case 35:  c[0] = 0.8f; c[1] = 0.8f; c[2] = 0.8f; break; // Alternate material
        case 42:  c[0] = 0.7f; c[1] = 0.7f; c[2] = 0.7f; break; // main color
        case 43:  c[0] = 0.8f; c[1] = 0.2f; c[2] = 0.0f; break; // wool
        case 44:  c[0] = 0.3f; c[1] = 0.3f; c[2] = 0.3f; break; // Stairs A
        case 45:  c[0] = 0.5f; c[1] = 0.5f; c[2] = 0.5f; break; // Stairs B
        case 47:  c[0] = 1.0f; c[1] = 1.0f; c[2] = 1.0f; break; // Dunno
        case 48:  c[0] = 1.0f; c[1] = 0.0f; c[2] = 0.0f; break; // Dunno
        case 53:  c[0] = 1.0f; c[1] = 1.0f; c[2] = 1.0f; break; // Dunno
        case 54:  c[0] = 1.0f; c[1] = 0.0f; c[2] = 0.0f; break; // Dunno
        case 58:  c[0] = 1.0f; c[1] = 1.0f; c[2] = 1.0f; break; // Dunno
        case 61:  c[0] = 1.0f; c[1] = 1.0f; c[2] = 1.0f; break; // Dunno
        case 64:  c[0] = 1.0f; c[1] = 1.0f; c[2] = 1.0f; break; // Dunno
        case 67:  c[0] = 0.3f; c[1] = 0.3f; c[2] = 0.3f; break; // main color
        case 68:  c[0] = 1.0f; c[1] = 1.0f; c[2] = 1.0f; break; // waste bin?
        case 70:  c[0] = 1.0f; c[1] = 1.0f; c[2] = 0.0f; break; // waste bin?
        case 76:  c[0] = 0.6f; c[1] = 0.6f; c[2] = 0.6f; break; // roof detail
        case 78:  c[0] = 0.7f; c[1] = 0.7f; c[2] = 0.7f; break; // roof detail
        case 82:  c[0] = 0.0f; c[1] = 0.8f; c[2] = 0.0f; break; // Cactus
        case 85:  c[0] = 0.5f; c[1] = 0.3f; c[2] = 0.1f; break; // Wood
        case 86:  c[0] = 0.5f; c[1] = 0.3f; c[2] = 0.1f; break; // Wood
        case 89:  c[0] = 1.0f; c[1] = 1.0f; c[2] = 1.0f; break; // Dunno
        case 90:  c[0] = 1.0f; c[1] = 1.0f; c[2] = 1.0f; break; // Dunno
        case 99: c[0] = 0.5f; c[1] = 0.5f; c[2] = 0.5f; break; // stone
        case 101:  c[0] = 0.5f; c[1] = 0.5f; c[2] = 0.5f; break; // Stairs
        case 110:  c[0] = 0.5f; c[1] = 0.5f; c[2] = 0.5f; break; // Stairs
        default: c[0] = 1.0f; c[1] = 1.0f; c[2] = 1.0f; printf("%d ", material);break;
    }
    memcpy(color, c, sizeof(c));
}

// load points and tetrahedrons and create OpenGL buffers
void createBuffers(char **argv) {
    printf("Reading points...");
    readArray(&pts, pts_dims, argv[1]);
    printf(" done\n");

    printf("Reading tetrahedrons...");
    readArray(&tetr, tetr_dims, argv[2]);
    printf(" done\n");

    printf("Reading displacement...");
    readArray(&displacement, displacement_dims, argv[3]);
    printf(" done\n");

    printf("Reading stress...");
    readArray(&stress, stress_dims, argv[4]);
    printf(" done\n");


    // First, process the data to a form that OpenGL likes
    // Remember that tetr uses 1-indexing, but pts uses 0-indexing.

    // 4 faces per tetrahedron, 3 points per face. 12 points per face. Three floats in a point.
    // In total, 36 floats per tetrahedron.
    vertexPos = (float*)calloc(36 * tetr_dims[1], sizeof(float)); // calloc zero fills
    vertexNormal = (float*)calloc(36 * tetr_dims[1], sizeof(float)); // calloc zero fills
    vertexDisplacement = (float*)calloc(36 * tetr_dims[1], sizeof(float));
    vertexColor = (float*)calloc(36 * tetr_dims[1], sizeof(float));
    vertexStress = (float*)calloc(12 * tetr_dims[1], sizeof(float));

    if (vertexStress == NULL) { printf("Out of memory :(\n"); exit(1); }

    // Loop trough tetrahedrons
    printf("Tetr dims = (%d x %d)\n", tetr_dims[0], tetr_dims[1]);
    printf("Pts dims  = (%d x %d)\n", pts_dims[0], pts_dims[1]);
    printf("Displacement dims  = (%d x %d)\n", displacement_dims[0], displacement_dims[1]);
    printf("Stress dims = (%d x %d)\n", stress_dims[0], stress_dims[1]);
    printf("Building vertex buffer and normals\n");
    for (int it = 0; it < tetr_dims[1]; it++) {

        // Get indices for the points  in this tetrahedron
        int n0 = (unsigned int)(tetr[it*tetr_dims[0] + 0] + 0.5f) - 1;
        int n1 = (unsigned int)(tetr[it*tetr_dims[0] + 1] + 0.5f) - 1;
        int n2 = (unsigned int)(tetr[it*tetr_dims[0] + 2] + 0.5f) - 1;
        int n3 = (unsigned int)(tetr[it*tetr_dims[0] + 3] + 0.5f) - 1;

        // Get material type and color if there is one
        unsigned int mat = 0;
        vec3 color;
        if (tetr_dims[0] >= 5) {
            mat = (unsigned int)(tetr[it*tetr_dims[0] + 4] + 0.5f);
        }
        getMaterialColor(color, mat);

        // Get coordinates as vec3 compatible pointers
        float* p0 = &pts[n0 * pts_dims[0]];
        float* p1 = &pts[n1 * pts_dims[0]];
        float* p2 = &pts[n2 * pts_dims[0]];
        float* p3 = &pts[n3 * pts_dims[0]];

        // Get displacement vectors
        float* d0 = &displacement[n0 * pts_dims[0]];
        float* d1 = &displacement[n1 * pts_dims[0]];
        float* d2 = &displacement[n2 * pts_dims[0]];
        float* d3 = &displacement[n3 * pts_dims[0]];

        // Get stress level
        float s0 = stress[n0];
        float s1 = stress[n1];
        float s2 = stress[n2];
        float s3 = stress[n3];

        // Temp variable
        vec3 normal;

        // Find the center of this tetrahedron. It will be used to make sure
        // that the normals are pointing away from it.
        vec3 tmp1; vec3 tmp2;
        vec3 pc; 
        vec3_add(tmp1, p0, p1); vec3_add(tmp2, p2, p3); vec3_add(pc, tmp1, tmp2);
        vec3_scale(pc, pc, 1.0f/4);

        // Vectors from center to corners
        vec3 pcp0; vec3_sub(pcp0, p0, pc);
        vec3 pcp1; vec3_sub(pcp1, p1, pc);
        vec3 pcp2; vec3_sub(pcp2, p2, pc);
        vec3 pcp3; vec3_sub(pcp3, p3, pc);

        /*
        printf("pc: %f %f %f\n", pc[0], pc[1], pc[2]);
        printf("p0: %f %f %f\n", p0[0], p0[1], p0[2]);
        printf("p1: %f %f %f\n", p1[0], p1[1], p1[2]);
        printf("p2: %f %f %f\n", p2[0], p2[1], p2[2]);
        printf("p3: %f %f %f\n", p3[0], p3[1], p3[2]);
        printf("pcp0: %f %f %f\n", pcp0[0], pcp0[1], pcp0[2]);
        */

        // Calculate vectors between points
        vec3 p0p1; vec3_sub(p0p1, p1, p0);
        vec3 p0p2; vec3_sub(p0p2, p2, p0);
        vec3 p0p3; vec3_sub(p0p3, p3, p0);

        vec3 p1p0; vec3_sub(p1p0, p0, p1);
        vec3 p1p2; vec3_sub(p1p2, p2, p1);
        vec3 p1p3; vec3_sub(p1p3, p3, p1);

        vec3 p2p0; vec3_sub(p2p0, p0, p2);
        vec3 p2p1; vec3_sub(p2p1, p1, p2);
        vec3 p2p3; vec3_sub(p2p3, p3, p2);

        vec3 p3p0; vec3_sub(p3p0, p0, p3);
        vec3 p3p1; vec3_sub(p3p1, p1, p3);
        vec3 p3p2; vec3_sub(p3p2, p2, p3);
        
        // First triangle p0 p1 p2
        // Position
        memcpy(&vertexPos[36*it + 0*9 + 0], p0, 3*sizeof(float));
        memcpy(&vertexPos[36*it + 0*9 + 3], p1, 3*sizeof(float));
        memcpy(&vertexPos[36*it + 0*9 + 6], p2, 3*sizeof(float));
        // Normal
        vec3_mul_cross(normal, p0p1, p0p2);
        if (vec3_mul_inner(normal, pcp0) < 0) { vec3_scale(normal, normal, -1);}
        memcpy(&vertexNormal[36*it + 0*9 + 0], normal, 3*sizeof(float));
        memcpy(&vertexNormal[36*it + 0*9 + 3], normal, 3*sizeof(float));
        memcpy(&vertexNormal[36*it + 0*9 + 6], normal, 3*sizeof(float));
        // Displacement
        memcpy(&vertexDisplacement[36*it + 0*9 + 0], d0, 3*sizeof(float));
        memcpy(&vertexDisplacement[36*it + 0*9 + 3], d1, 3*sizeof(float));
        memcpy(&vertexDisplacement[36*it + 0*9 + 6], d2, 3*sizeof(float));
        // Color
        memcpy(&vertexColor[36*it + 0*9 + 0], color, sizeof(color));
        memcpy(&vertexColor[36*it + 0*9 + 3], color, sizeof(color));
        memcpy(&vertexColor[36*it + 0*9 + 6], color, sizeof(color));
        // Stress
        vertexStress[12*it + 0*3 + 0] = s0;
        vertexStress[12*it + 0*3 + 1] = s0;
        vertexStress[12*it + 0*3 + 2] = s0;

        // Second triangle p0 p1 p3
        // Position
        memcpy(&vertexPos[36*it + 1*9 + 0], p0, 3*sizeof(float));
        memcpy(&vertexPos[36*it + 1*9 + 3], p1, 3*sizeof(float));
        memcpy(&vertexPos[36*it + 1*9 + 6], p3, 3*sizeof(float));
        // Normal
        vec3_mul_cross(normal, p0p1, p0p3);
        if (vec3_mul_inner(normal, pcp0) < 0) { vec3_scale(normal, normal, -1);}
        memcpy(&vertexNormal[36*it + 1*9 + 0], normal, 3*sizeof(float));
        memcpy(&vertexNormal[36*it + 1*9 + 3], normal, 3*sizeof(float));
        memcpy(&vertexNormal[36*it + 1*9 + 6], normal, 3*sizeof(float));
        // Displacement
        memcpy(&vertexDisplacement[36*it + 1*9 + 0], d0, 3*sizeof(float));
        memcpy(&vertexDisplacement[36*it + 1*9 + 3], d1, 3*sizeof(float));
        memcpy(&vertexDisplacement[36*it + 1*9 + 6], d3, 3*sizeof(float));
        // Color
        memcpy(&vertexColor[36*it + 1*9 + 0], color, sizeof(color));
        memcpy(&vertexColor[36*it + 1*9 + 3], color, sizeof(color));
        memcpy(&vertexColor[36*it + 1*9 + 6], color, sizeof(color));
        // Stress
        vertexStress[12*it + 1*3 + 0] = s1;
        vertexStress[12*it + 1*3 + 1] = s1;
        vertexStress[12*it + 1*3 + 2] = s1;

        // Third triangle p0 p2 p3
        // Position
        memcpy(&vertexPos[36*it + 2*9 + 0], p0, 3*sizeof(float));
        memcpy(&vertexPos[36*it + 2*9 + 3], p2, 3*sizeof(float));
        memcpy(&vertexPos[36*it + 2*9 + 6], p3, 3*sizeof(float));
        // Normal
        vec3_mul_cross(normal, p0p2, p0p3);
        if (vec3_mul_inner(normal, pcp0) < 0) { vec3_scale(normal, normal, -1);}
        memcpy(&vertexNormal[36*it + 2*9 + 0], normal, 3*sizeof(float));
        memcpy(&vertexNormal[36*it + 2*9 + 3], normal, 3*sizeof(float));
        memcpy(&vertexNormal[36*it + 2*9 + 6], normal, 3*sizeof(float));
        // Displacement
        memcpy(&vertexDisplacement[36*it + 2*9 + 0], d0, 3*sizeof(float));
        memcpy(&vertexDisplacement[36*it + 2*9 + 3], d2, 3*sizeof(float));
        memcpy(&vertexDisplacement[36*it + 2*9 + 6], d3, 3*sizeof(float));
        // Color
        memcpy(&vertexColor[36*it + 2*9 + 0], color, sizeof(color));
        memcpy(&vertexColor[36*it + 2*9 + 3], color, sizeof(color));
        memcpy(&vertexColor[36*it + 2*9 + 6], color, sizeof(color));
        // Stress
        vertexStress[12*it + 2*3 + 0] = s2;
        vertexStress[12*it + 2*3 + 1] = s2;
        vertexStress[12*it + 2*3 + 2] = s2;

        // Fourth triangle p1 p2 p3
        // Position
        memcpy(&vertexPos[36*it + 3*9 + 0], p1, 3*sizeof(float));
        memcpy(&vertexPos[36*it + 3*9 + 3], p2, 3*sizeof(float));
        memcpy(&vertexPos[36*it + 3*9 + 6], p3, 3*sizeof(float));
        // Normal
        vec3_mul_cross(normal, p1p2, p1p3);
        if (vec3_mul_inner(normal, pcp1) < 0) { vec3_scale(normal, normal, -1);}
        memcpy(&vertexNormal[36*it + 3*9 + 0], normal, 3*sizeof(float));
        memcpy(&vertexNormal[36*it + 3*9 + 3], normal, 3*sizeof(float));
        memcpy(&vertexNormal[36*it + 3*9 + 6], normal, 3*sizeof(float));
        // Displacement
        memcpy(&vertexDisplacement[36*it + 3*9 + 0], d1, 3*sizeof(float));
        memcpy(&vertexDisplacement[36*it + 3*9 + 3], d2, 3*sizeof(float));
        memcpy(&vertexDisplacement[36*it + 3*9 + 6], d3, 3*sizeof(float));
        // Color
        memcpy(&vertexColor[36*it + 3*9 + 0], color, sizeof(color));
        memcpy(&vertexColor[36*it + 3*9 + 3], color, sizeof(color));
        memcpy(&vertexColor[36*it + 3*9 + 6], color, sizeof(color));
        // Stress
        vertexStress[12*it + 3*3 + 0] = s3;
        vertexStress[12*it + 3*3 + 1] = s3;
        vertexStress[12*it + 3*3 + 2] = s3;
    }

    printf("Copying vertexPos buffer...\n");
    // Create buffer and copy vertices
    glGenBuffers(1, &vertexPosBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexPosBuffer);
    glBufferData(GL_ARRAY_BUFFER, 36 * sizeof(float) * tetr_dims[1], vertexPos, GL_STATIC_DRAW);
    // Set options for drawing vertices
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glDisableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);


    printf("Copying vertexNormal buffer...\n");
    // Create buffer and copy normals
    glGenBuffers(1, &vertexNormalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexNormalBuffer);
    glBufferData(GL_ARRAY_BUFFER, 36 * sizeof(float) * tetr_dims[1], vertexNormal, GL_STATIC_DRAW);
    // Set options for normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glDisableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, 0);


    printf("Copying vertexDisplacement buffer...\n");
    // Create buffer and copy displacement vectors
    glGenBuffers(1, &vertexDisplacementBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexDisplacementBuffer);
    glBufferData(GL_ARRAY_BUFFER, 36 * sizeof(float) * tetr_dims[1], vertexDisplacement, GL_STATIC_DRAW);
    // Set options for displacement vectors
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glDisableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, 0);


    printf("Copying color buffer...\n");
    // Create buffer and copy color vectors
    glGenBuffers(1, &vertexColorBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexColorBuffer);
    glBufferData(GL_ARRAY_BUFFER, 36 * sizeof(float) * tetr_dims[1], vertexColor, GL_STATIC_DRAW);
    // Set options for color vectors
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glDisableVertexAttribArray(3);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    printf("Copying stress buffer...\n");
    // Create buffer and copy stress floats
    glGenBuffers(1, &vertexStressBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexStressBuffer);
    glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(float) * tetr_dims[1], vertexStress, GL_STATIC_DRAW);
    // Set options for color floats
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, 0, NULL);

    glDisableVertexAttribArray(4);
    glBindBuffer(GL_ARRAY_BUFFER, 0);


}

int main(int argc, char **argv)
{
    if (argc != 5) {
        printf("Usage: read_mat pts.mat tetr.mat displacement.mat stress.mat\n");
        exit(0);
    }

    // pts
    // The first three values are the coordinates to a point.
    // The rest are ignored.
    // x1 y1 z1 ...
    // x2 y2 z2 ...
    //
    // tetr
    // Four first values are indices to the rows in the points array.
    // The fifth is the material type [optional]
    // The rest are ignored.
    // 1 2 3 4 5 ...
    // 2 3 4 5 6 ...
    //
    // disp
    // The first three values are the displacement vector components
    // The rest are ignored
    // dx1 dy1 dz1
    // dx2 dy2 dz2

    initGL(&argc, argv);

    createBuffers(argv);

    // Debug: print first tetrahedron points
    printf("The first points are\n");
    for (int i = 0; i < 36; i++) {
        if (i % 3 == 0) { printf("\n"); }
        if (i % 9 == 0) { printf("\n"); }
        printf("    %f ", vertexPos[i]);
    }
    printf("\n");


    glutMainLoop();


    // Todo:
    // Free points
    // Free tetrahedrons
    return 0;

}
