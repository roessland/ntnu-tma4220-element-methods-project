/*
 * Reads a MAT file and dumps the contents to console
 *
 * Usage:
 *
 *    read_mat <matfile>
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <mex.h>
#include <mat.h>
#include <matrix.h>

// Threading
#include <pthread.h>

// OpenGL headers
#include <GL/glew.h>
#include <GL/glut.h>

// mexFlush forces mexPrintf output to be flushed to MATLAB immediately
void mexFlush()
{
    mexEvalString("drawnow");
}

// display is the display-loop
void displayLoop(void)
{
    glClearColor(0.5f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glutSwapBuffers();
    glutPostRedisplay();
    pthread_exit(NULL);
}

// initGL sets up glut and launches the display loop
void initGL(int* argc, char** argv)
{
    glutInit(argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glDepthFunc(GL_LESS);
    glutInitWindowSize(400, 400);
    glutCreateWindow("MATLAB OpenGL viewer");
    glutDisplayFunc(displayLoop);
    glewInit();
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
}

// displayThread opens the OpenGL window
void *displayThread(void *arg)
{
    mexPrintf("Initiating OpenGL.\n");
    // glutInit demands some parameters, so just give it some dummy arguments
    char fakeParam[] = "fake";
    char *fakeArgv[] = { fakeParam, NULL };
    int fakeArgc = 1;
    initGL(&fakeArgc, fakeArgv);
    pthread_exit(NULL);
}



void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    pthread_t thread;
    int err;

    // Create OpenGL thread
    mexPrintf("Creating thread.\n");
    mexFlush();
    err = pthread_create(&thread, NULL, displayThread, NULL);
    if (err) {
        mexErrMsgTxt("Couldn't create thread!\n");
    }

    // Wait for it to finish
    pthread_join(thread, NULL);
}

/* Hello world!
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    mexPrintf("Hello World!\n");
    usleep(100000000);
}
*/
