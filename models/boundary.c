#include <stdio.h>
#include <math.h>
#define MAX 10000000
double px[MAX],py[MAX],pz[MAX];
int pn;
int tri[MAX][4];
int tn;

void loadpoints(char *s) {
	FILE *f=fopen(s,"r");
	double x,y,z;
	pn=0;
	if(!f) puts("error");
	while(fscanf(f,"%lf %lf %lf",&x,&y,&z)==3) {
		px[pn]=x;
		py[pn]=y;
		pz[pn++]=z;
	}
	fclose(f);
}

void loadtri(char *s) {
	FILE *f=fopen(s,"r");
	int a,b,c,d;
	tn=0;
	if(!f) puts("error");
	while(fscanf(f,"%d %d %d %d",&a,&b,&c,&d)==4) {
		tri[tn][0]=a-1;
		tri[tn][1]=b-1;
		tri[tn][2]=c-1;
		tri[tn++][3]=d-1;
	}
	fclose(f);
}

void findboundary() {
	int i,c,j,z,p[4];
	for(i=0;i<tn;i++) {
		for(c=j=0;j<4;j++) {
			z=tri[i][j];
			if(fabs(pz[z])<1e-8) p[c++]=z;
		}
		if(c==3) printf("%d %d %d\n",p[0]+1,p[1]+1,p[2]+1);
	}
}

/* parameters: points, triangles */
int main(int argc,char **argv) {
	puts("find boundary at z=0");
	if(argc<3) { printf("boundary pointfile trifile\n"); return 0; }
	loadpoints(argv[1]);
	loadtri(argv[2]);
	printf("read %d points, %d elements\n",pn,tn);
	findboundary();
	return 0;
}
