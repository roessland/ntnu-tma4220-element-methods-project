function A=triarea(p1,p2,p3)
% function A = triarea(p1,p2,p3)
%
% description:
%      calculate area of triangle
%
% arguments:
%   - p1,p2,p3 corners of triangle
%
% returns:
%   - A        area of triangle
A=0.5*abs(det([p1-p3;p2-p3]));
