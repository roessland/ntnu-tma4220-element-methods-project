function I=quadrature2D(p1,p2,p3,Nq,g)
% function I = quadrature2D(p1,p2,p3,Nq,g)
%
% description:
%      evaluate integral
%
% arguments:
%   - p1,p2,p3 corners of triangle (row vectors with 2 elements)
%   - Nq       number of quadrature points (1, 3 or 4)
%   - g        pointer to function [R R] -> R
%
% returns:
%   - I        value of the integral

if Nq==1
    c=[1/3 1/3 1/3];
    p=[1];
elseif Nq==3
    c=[1/2 1/2 0; 1/2 0 1/2; 0 1/2 1/2];
    p=[1/3 1/3 1/3];
elseif Nq==4
    c=[1/3 1/3 1/3; 3/5 1/5 1/5; 1/5 3/5 1/5; 1/5 1/5 3/5];
    p=[-9/16 25/48 25/48 25/48];
else
    error('illegal Nq value');
end
I=0;
P=[p1;p2;p3];
A=triarea(p1,p2,p3);
for q=1:Nq
    x=c(q,:)*P;
    I=I+p(q)*g(x)*A;
end
