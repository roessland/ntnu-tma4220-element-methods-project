function I=quadrature3D(p1,p2,p3,p4,Nq,g)
% function I = quadrature3D(p1,p2,p3,p4,Nq,g)
%
% description:
%      evaluate integral
%
% arguments:
%   - p1,p2,p3,p4 corners of tetrahedron (row vectors with 3 elements)
%   - Nq          number of quadrature points (1, 4 or 5)
%   - g           pointer to function [R R R] -> R
%
% returns:
%   - I        value of the integral

if Nq==1
    c=[1/4 1/4 1/4 1/4];
    p=[1];
elseif Nq==4
    c=[0.5854102, 0.1381966, 0.1381966, 0.1381966;
       0.1381966, 0.5854102, 0.1381966, 0.1381966;
       0.1381966, 0.1381966, 0.5854102, 0.1381966;
       0.1381966, 0.1381966, 0.1381966, 0.5854102]; 
    p=[1/4 1/4 1/4 1/4];
elseif Nq==5
    c=[1/4 1/4 1/4 1/4;
       1/2 1/6 1/6 1/6;
       1/6 1/2 1/6 1/6;
       1/6 1/6 1/2 1/6;
       1/6 1/6 1/6 1/2];
    p=[-4/5 9/20 9/20 9/20 9/20];
else
    error('illegal Nq value');
end
I=0;
P=[p1;p2;p3;p4];
V=tetravolume(p1,p2,p3,p4);
for q=1:Nq
    x=c(q,:)*P;
    I=I+p(q)*g(x)*V;
end
