function I=quadrature1D_line(a,b,Nq,g)
% function I = quadrature1D_line(a,b,Nq,g)
%
% description:
%      evaluate line integral
%
% arguments:
%   - a,b   limits of integral (2D points)
%   - Nq    number of quadrature points (1, 2, 3 or 4)
%   - g     pointer to function of two variables
%
% returns:
%   - I     value of the integral

if Nq==1
    z=[0];
    p=[2];
elseif Nq==2
    z=[-sqrt(1/3) sqrt(1/3)];
    p=[1 1];
elseif Nq==3
    z=[-sqrt(3/5) 0 sqrt(3/5)];
    p=[5/9 8/9 5/9];
elseif Nq==4
    z=[-sqrt((3+2*sqrt(6/5))/7) -sqrt((3-2*sqrt(6/5))/7) sqrt((3-2*sqrt(6/5))/7) sqrt((3+2*sqrt(6/5))/7)];
    p=[(18-sqrt(30))/36 (18+sqrt(30))/36 (18+sqrt(30))/36 (18-sqrt(30))/36];
else
    error('illegal Nq value');
end
I=0;
scale=norm(b-a)/2;
dir=b-a;
for q=1:Nq
    I=I+p(q)*g((b+a+dir*z(q))/2)*scale;
end
