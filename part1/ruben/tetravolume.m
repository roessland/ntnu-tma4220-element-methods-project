function V=tetravolume(p1,p2,p3,p4)
% function V = tetravolume(p1,p2,p3,p4)
%
% description:
%      calculate volume of tetrahedron
%
% arguments:
%   - p1,p2,p3,p4 corners of tetrahedron (row vectors of 3)
%
% returns:
%   - V           volume of tetrahedron
V=abs(det([p1-p4; p2-p4; p3-p4]))/6;
