function I=quadrature1D(a,b,Nq,g)
% function I = quadrature1D(a,b,Nq,g)
%
% description:
%      evaluate integral
%
% arguments:
%   - a,b   limits of integral
%   - Nq    number of quadrature points (1, 2, 3 or 4)
%   - g     pointer to function
%
% returns:
%   - I     value of the integral

if Nq==1
    z=[0];
    p=[2];
elseif Nq==2
    z=[-sqrt(1/3) sqrt(1/3)];
    p=[1 1];
elseif Nq==3
    z=[-sqrt(3/5) 0 sqrt(3/5)];
    p=[5/9 8/9 5/9];
elseif Nq==4
    z=[-sqrt((3+2*sqrt(6/5))/7) -sqrt((3-2*sqrt(6/5))/7) sqrt((3-2*sqrt(6/5))/7) sqrt((3+2*sqrt(6/5))/7)];
    p=[(18-sqrt(30))/36 (18+sqrt(30))/36 (18+sqrt(30))/36 (18-sqrt(30))/36];
else
    error('illegal Nq value');
end
I=0;
for q=1:Nq
    % scale argument to g() from [a,b] to [-1,1] and scale return
    % value back to [a,b]
    I=I+p(q)*g((b-(b+a)/2)*z(q)+(b+a)/2)*(b-a)/2;
end
