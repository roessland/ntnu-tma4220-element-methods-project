function task4(n)
% function task4(n)
%
% description:
%      poisson in 2d with neumann
%
% arguments:
%   - n     number of nodes, at least 4

[p tri edge] = getSphere(n);
K=size(tri,1);  % K: number of elements

% our function, here converted to non-polar form
g=@(x) -12*pi*cos(2*pi*(x(1)^2+x(2)^2+x(3)^2))+16*pi^2*(x(1)^2+x(2)^2+x(3)^2)*sin(2*pi*(x(1)^2+x(2)^2+x(3)^2));

% create A and f, see LN-4 page 15
% in short, loop over the elements rather than naively looping over all
% pairs of global nodes i,j and checking if they belong to the same element
A=zeros(n,n);
f=zeros(n,1);
for k=1:K
    % find basis functions and put coefficients in c, see LN-4 pages 12-13
    c=zeros(4,4);
    lhs=[1 p(tri(k,1),1) p(tri(k,1),2) p(tri(k,1),3);
          1 p(tri(k,2),1) p(tri(k,2),2) p(tri(k,2),3);
          1 p(tri(k,3),1) p(tri(k,3),2) p(tri(k,3),3);
          1 p(tri(k,4),1) p(tri(k,4),2) p(tri(k,4),3)];
    for alpha=1:4
        rhs=zeros(4,1);
        rhs(alpha)=1;
        c(alpha,:)=lhs\rhs;
    end
    volume=tetravolume(p(tri(k,1),:),p(tri(k,2),:),p(tri(k,3),:),p(tri(k,4),:));
    for alpha=1:4
        i=tri(k,alpha);
        for beta=1:4
            j=tri(k,beta);
            % TODO change the next line to 3d
            A(i,j)=A(i,j)+volume*(c(alpha,2)*c(beta,2)+c(alpha,3)*c(beta,3)+c(alpha,4)*c(beta,4));
        end
        h=@(x) g(x)*(c(alpha,1)+c(alpha,2)*x(1)+c(alpha,3)*x(2))+c(alpha,4)*x(3);
        f(i)=f(i)+quadrature3D(p(tri(k,1),:),p(tri(k,2),:),p(tri(k,3),:),p(tri(k,4),:),4,h);
    end
end

% boundary conditions!
% use "big number" approach described in LN-3 page 28
% set A_ii=1<<eps and F_i=0 for boundary nodes
big=1e7;
for k=1:n
    if p(k,1)^2+p(k,2)^2+p(k,3)^2+1e-6>1
        A(k,k)=big;
        f(k)=0;
    end
end
u=A\f;

% exact solution
uexact=zeros(n,1);
for i=1:n
    x=p(i,1);
    y=p(i,2);
    z=p(i,3);
    uexact(i)=sin(2*pi*(x^2+y^2+z^2));
end
% TODO plot
sprintf('maximal error is %.10f',max(abs(u-uexact)))
