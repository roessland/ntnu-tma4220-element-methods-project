% test quadrature1D
f=@(x) exp(x);
sprintf('1D 1 point : %.10f',quadrature1D(1,2,1,f))
sprintf('1D 2 points: %.10f',quadrature1D(1,2,2,f))
sprintf('1D 3 points: %.10f',quadrature1D(1,2,3,f))
sprintf('1D 4 points: %.10f',quadrature1D(1,2,4,f))
sprintf('1D exact sol %.10f',exp(2)-exp(1))

% test quadrature2D
g=@(x) log(x(1)+x(2));
sprintf('2D 1 point : %.10f',quadrature2D([1 0],[3 1],[3 2],1,g))
sprintf('2D 3 points: %.10f',quadrature2D([1 0],[3 1],[3 2],3,g))
sprintf('2D 4 points: %.10f',quadrature2D([1 0],[3 1],[3 2],4,g))
sprintf('2D exact sol %.10f',(75*log(5)-64*log(4)-18)/12)

% test quadrature3D
h=@(x) exp(x(1));
sprintf('3D 1 point : %.10f',quadrature3D([0 0 0],[0 2 0],[0 0 2],[2 0 0],1,h))
sprintf('3D 4 points: %.10f',quadrature3D([0 0 0],[0 2 0],[0 0 2],[2 0 0],4,h))
sprintf('3D 5 points: %.10f',quadrature3D([0 0 0],[0 2 0],[0 0 2],[2 0 0],5,h))
sprintf('3D exact sol %.10f',exp(2)-5)
