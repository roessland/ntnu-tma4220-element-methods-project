function [ I ] = quadrature2D( p1, p2, p3, Nq, g )
%I = QUADRATURE2D(p1,p2,p3,Nq,g) Integrates a function over a triangle
%element using Gaussian quadrature with the 1, 3 or 4 point rules.
%
%   I \in R          value of the integral
%   p1 \in R^2       first corner point of the triangle (vertical matrix)
%   p2 \in R^2       second corner point of the triangle (vertical matrix)
%   p3 \in R^2       third corner point of the triangle (vertical matrix)
%   Nq \in {1,3,4}   number of integration points
%   g: R^2 \to R     function pointer (must take vertical matrix)
%


% Set quadrature parameters
%
% wq = weights
% zq = quadrature points in barycentric coordinates

if Nq == 1
    % 1-point rule
    wq = [1];
    Zq = [1/3 1/3 1/3];
elseif Nq == 3
    % 3-point rule
    wq = [1/3
          1/3
          1/3];
      
    Zq = [1/2  1/2   0;
          1/2   0   1/2;
           0   1/2  1/2];   
elseif Nq == 4
    % 4-point rule
    wq = [-9/16;
          25/48;
          25/48;
          25/48];
      
    Zq = [1/3 1/3 1/3;
          3/5 1/5 1/5;
          1/5 3/5 1/5;
          1/5 1/5 3/5];
else
    error('Only the 1, 3 and 4 point rules are supported');
end

% Transform quadrature points from barycentric coordinates to physical
% coordinates, using x = z1 p1 + z2 p2 + z3 p3.
P = [p1 p2 p3];
Xq = P * Zq';

% Compute quadrature
I = 0;
for n = 1:Nq
    I = I + wq(n) * g(Xq(:,n));
end

% This was for a triangle with area 1, so we have to multiply with the are
% of the real triangle.
A = triangle_area(p1, p2, p3);
I = I * A;

end

