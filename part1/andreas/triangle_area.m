function [ area ] = triangle_area( p1, p2, p3 )
% TRIANGLE_AREA Returns the area between three points
% p1,p2,p3 must be vertical XY/XYZ vectors.

% Vertical vectors only...
assert(size(p1,2) == 1);

if (size(p1,1) == 2)
    area = 1/2*abs( det([p3-p1 p2-p1]) );
elseif (size(p1,1) == 3)
    area = 1/2*norm( cross(p3-p1, p2-p1) );
end

end