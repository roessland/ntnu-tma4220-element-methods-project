clear all
% Poisson solver in 3D
% See
% http://www.math.ntnu.no/emner/TMA4220/2013h/notes/TMA4220_2007-Lecture_Note_4.pdf
% for details.

% Variables
%   - Nn    the number of nodes in the mesh
%   - f     a function from R^3 to R for the inner condition.
%   - g     a function from R^3 to R for the boundary condition.
Nn = 4000;

% The f given for this task, and the analytic solution
f = @(x) -12*pi*cos(2*pi*norm(x)^2) + 16*pi^2*norm(x)^2*sin(2*pi*norm(x)^2);
%u_analytic = @(x) sin(pi*norm(x)^2);

% This is the boundary condition for use in the Neumann case
%g = @(x) 4*pi*(x(1)^2 + x(2)^2) * cos(2*pi*(x(1)^2 + x(2)^2));



% Ensure the getDisk() function is in path
addpath('grids');

% Generate a disk mesh
%   - p       nodal points. (x,y)-coordinates for point i given in row i.
%   - tet     elements. Index to the three corners of element i given in row i.
%   - surface surface triangles. 3 point indexes on each row
[p tet surface] = getSphere(Nn);

% Build stiffness matrix Ah and load vector Fh
% Look trough all elements, and add contributions to the nodes.
%   - K    the number of elements in the mesh
K = size(tet,1);
Ah = zeros(Nn,Nn);
Fh = zeros(Nn,1);

% Get all boundary nodes, Dirichlet nodes, Neumann nodes and inner nodes.
% This will be used later to remove the Dirichlet boundary, and to check if
% a node must satisfy a Neumann condition.
%
% IMPORTANT! inner_nodes include the Neumann nodes, but NOT the Dirichlet
% nodes.
all_nodes = 1:Nn;
boundary_nodes = unique(surface);
dirichlet_nodes = boundary_nodes; % Homogeneous case
%dirichlet_nodes = boundary_nodes(p(boundary_nodes,2) < 0); % Mixed case
neumann_nodes = setdiff(boundary_nodes, dirichlet_nodes);
inner_nodes = setdiff(all_nodes, dirichlet_nodes);

assert(length(dirichlet_nodes) + length(inner_nodes) == Nn);

% Unit vectors in R^4.
% Used for constructing the basis function (4D) planes on an element.
e = [1 0 0 0;
     0 1 0 0;
     0 0 1 0
     0 0 0 1];

for k = 1:K
    % The corner numbers for this element
    tetk = tet(k,:);
    
    % The corner positions for this element. Each point is a row.
    pk = [p(tetk(1),:);
          p(tetk(2),:);
          p(tetk(3),:);
          p(tetk(4),:)];
      
    % The volume of this element with the usual determinant formula.
    volume = tetrahedron_volume(pk(1,:)', pk(2,:)', pk(3,:)', pk(4,:)');
      
    % The matrix used to solve for the coefficients of the basis functions
    % on element k. (They are four dimensional planes)
    Mk = [[1;1;1;1] pk];
    
    % Find all the basis functions on this element. The alpha'th column has
    % the coefficients for the plane for corner alpha. Phi on the alpha'th
    % corner of element k is given by the plane Hk(alpha) = ckalpha(1) +
    % ckalpha(2)*x + ckalpha(3)*y + ckalpha(4)*z. x, y, z and Hk is known at
    % the corners, and thus we can solve for ck.
    ck = zeros(4,4);
    for alpha = 1:4
        ck(:,alpha) = Mk\e(:,alpha);
    end
    
    % Ak is the elemental 4x4 matrix for this element, as given at page 13
    % in Lecture note 4.
    Ak = @(alpha, beta) volume * (ck(2,alpha)*ck(2,beta) + ck(3,alpha)*ck(3,beta) + ck(4,alpha)*ck(4,beta));
    
    % Find nodes for element k that are also Neumann boundary nodes. This
    % will be used later when checking if we need to do a line integral
    % between boundary points.
    k_boundary = intersect(tetk, neumann_nodes);
    
    % The procedure for constructing Ah works by iterating over all
    % elements, and for each element constructing all the 16 possible
    % integrals of the basis functions. We have combinations
    % 111,221,333,123,132 etc. These integrals then contribute to the
    % corner points of the element.
    for alpha = 1:4
        i = tetk(alpha);
        
        % First, construct the load vector component using quadrature to
        % evaluate the integral. Hk is the 4D plane over this element,
        % defined so that Hk(p1) = 1, Hk(p2) = 0, Hk(p3) = 0, Hk(p4) = 0,
        % etc.
        
        % Fkalpha = integral of f * Hkalpha over the element
        Hk = @(x) ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2) + ck(4,alpha)*x(3); 
        
        % Test that the Hk plane is 1 at the current node
        assert((Hk(pk(alpha,:)) - 1) < 0.01);
        
        g_a = @(x) f(x) * Hk(x); % integrand
        Fkalpha_a = quadrature3D(pk(1,:)', pk(2,:)', pk(3,:)', pk(4,:)', 5, g_a);
        Fh(i) = Fh(i) + Fkalpha_a;
        
        % If point tetk(alpha) is on the Neumann boundary, we need to add a
        % line integral to the load vector element Fh(i).
        %
        % Possible cases:
        %
        % 1. Node tetk(alpha) is not on the boundary, Hk becomes zero on
        % the boundary, so the line integral is zero anyways, and thus
        % unnecessary.
        %
        % 2. Node tetk(alpha) is the only point on the boundary, the area
        % of the integral is zero, and thus the integral is zero, and thus
        % unnecessary.
        %
        % 3. Node tetk(alpha) and some neighbor is on the boudnary, the
        % area of the integral is still zero, and thus unnecessary.
        %
        % 4. Node tetk(alpha) is on the boundary, along with two other
        % neighbors. This means we have to do a surface integral over a
        % triangle.
        %
        % 5. Node tetk(alpha) is on the boundary, along with three other
        % neighbors. This should never happen, and we will throw an error
        % in this case.

        % Only add to load vector if trik(alpha) is on the boundary
        % TODO FIX THIS FOR NEUMANN CASE
%         
%         if ismember(tetk(alpha), k_boundary)
%             % Boundary points for element k that are not trik(alpha)
%             alpha_neighbors = setdiff(k_boundary, tetk(alpha));
%             
%             % Add line integrals for both case 3 and 4
%             g_b = @(x) g(x) * Hk(x); % integrand
%             for neighbor = alpha_neighbors
%                 Fkalpha_b = quadrature2D(p(trik(alpha),:)', p(neighbor,:)', 4, g_b);
%                 Fh(i) = Fh(i) + Fkalpha_b;
%             end
%         end
        
        % The remaining part is for Ah
        for beta = 1:4
            j = tetk(beta);
            Ah(i,j) = Ah(i,j) + Ak(alpha, beta);
        end

    end
end
%error('34545')

% The system is now singular, and cannot be solved. This is because we have
% too many basis functions. To fix this, we will remove all boundary
% points, because they are zero anyways.


% Select only the inner nodes of the stiffness matrix and the load vector.
% Nodes on the Dirichlet boundary will be added back later.
Ah_inner = Ah(inner_nodes, inner_nodes);
Fh_inner = Fh(inner_nodes);

% Solve the inner part
uh_inner = Ah_inner \ Fh_inner;

% Build u-values vector by setting all points to zero, so that the
% Dirichlet boundary condition is satisfied, and then setting the value of
% all the inner nodes, so that the other conditions are fulfulled.
u = zeros(Nn,1);
for i = 1:length(uh_inner)
    u(inner_nodes(i)) = uh_inner(i);
end

% Note: At this point the solution u is exactly the same as Johannes'
% solution in the file plotresult_3d.m. The load vector and stiffness
% matrix are also identical.



figure
tetramesh(tet,p,'FaceColor','red','EdgeColor','black');
camlight
lighting gouraud
xlabel('x')
ylabel('y')
zlabel('z')


%error('tralala');


% Plot error. This is the exact u for f given in the problem set.
% Currently very wrong...
figure
u_analytic = zeros(Nn, 1);
for i = 1:Nn
   pn = p(i,:);
   u_analytic(i) = sin(2*pi*norm(pn)^2);
end
plot(u);
hold on
plot(u_analytic);
legend('Error');

%error('Comment this line out to view the 3D plot (slow)');


% Plot our solution
figure

% Use our scattered point data to construct an interpolant. This is a 3D
% function F(x,y,z) that approximates our solution over the whole space
% using linear interpolation inside tetrahedrons. 
% Example, F(p(10,:)) = u(10), but F(0.5, 0, 0.3) will be interpolated.
F = TriScatteredInterp(p, u);

% Uncomment this to draw a ball of the domain, without shading
%tetramesh(tet, p);

% M is the amount of grid points in each direction of the axes,
% where we will sample the interpolant F.
M = 41;

% Create axes. Cut x in half so that we can see inside the sphere.
x = linspace(0, 1, M);
y = linspace(-1, 1, M);
z = linspace(-1, 1, M);

% Construct a three dimensional matrix v, which will hold the values of the
% interpolant at the grid points defined by the axes x, y and z. Is there a
% better way to do this without looping?
v = zeros(M, M, M);
for i = 1:M
    for j = 1:M
        for k = 1:M
            v(i,j,k) = F(x(i), y(j), z(k));
        end
    end
end

% Draw an isosurface for these isovalues
num_isosurfaces = 10; % Must be even

% Draw isosurfaces for iso = -1 to 1. Skip the area between -0.5 and 0.05,
% because those isosurfaces are really ugly and partially undefined.
iso_a = linspace(-1, -0.5, num_isosurfaces/2);
iso_b = linspace(0.05, 1, num_isosurfaces/2);
iso = [iso_a iso_b];

% Define a colormap to use. The "hot" colormap has 64 rows, of colors,
% where each row contains R, G and B values. Define one color for each
% isosurface.
cmap = colormap(hot);
num_colors = size(cmap, 1);

% Get one color for each isosurface
color = cmap(round(linspace(1, num_colors, num_isosurfaces)),:);

for i = 1:num_isosurfaces
    pp = patch(isosurface(x, y, z, v, iso(i)));
    isonormals(x, y, z, v, pp)
    set(pp,'FaceColor', color(i,:),'EdgeColor','none');
    %daspect([1,1,1])
    view(3); axis tight
    camlight 
    lighting gouraud
    hold on
end