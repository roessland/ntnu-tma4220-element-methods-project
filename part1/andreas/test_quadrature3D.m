function test_suite = test_quadrature3D
    initTestSuite
end


% Test that the integral is zero if the tetrahedron is degenerate,
% that is, if three or more points are on the same line.
function testZeroVolumeTetraHedron
    g = @(x) 1000;
    p1 = [1 1 1]'; p2 = [2 2 2]'; p3 = [3 3 3]'; p4 = [1 10 -3]';
    
    Ia = 0;
    
    Ib = quadrature3D(p1, p2, p3, p4, 1, g);
    assertEqual(Ia, Ib);
    
    Ib = quadrature3D(p1, p2, p3, p4, 4, g);
    assertEqual(Ia, Ib);
    
    Ib = quadrature3D(p1, p2, p3, p4, 5, g);
    assertEqual(Ia, Ib);
end

% Test that the integral of 1 is equal to the volume of the tetrahedron
function testVolumeEqualsIntegralOfOne
    g = @(x) 1;
    p1 = [0 0 1]'; p2 = [0 1 0]'; p3 = [1 0 0]'; p4 = [0 0 0]';
    
    % This is a pyramid. Area of the base * height * 1/3;
    Ia = 1/2 * 1 * 1/3;
    
    Ib = quadrature3D(p1, p2, p3, p4, 1, g);
    assertEqual(Ia, Ib);
    
    Ib = quadrature3D(p1, p2, p3, p4, 4, g);
    assertEqual(Ia, Ib);
    
    Ib = quadrature3D(p1, p2, p3, p4, 5, g);
    assertEqual(Ia, Ib);
end

% Test that integral of e^x dx dy dz is correct
function testExponentialIntegral
    g = @(x) exp(x(1));
    p1 = [0 0 0]'; p2 = [0 2 0]'; p3 = [0 0 2]'; p4 = [2 0 0]';
    
    Ia = 2.3844;
    
    Ib = quadrature3D(p1, p2, p3, p4, 1, g);
    assertElementsAlmostEqual(Ia, Ib, 'absolute', 0.2);
    
    Ib = quadrature3D(p1, p2, p3, p4, 4, g);
    assertElementsAlmostEqual(Ia, Ib, 'absolute', 0.01);
    
    Ib = quadrature3D(p1, p2, p3, p4, 5, g);
    assertElementsAlmostEqual(Ia, Ib, 'absolute', 0.001);
end

% Test that illegal point rules throw an exception
function testExceptionThrownWhenIllegalPointRule
    F = @() quadrature3D([0 1 0]', [1 0 0]', [0 0 0]', [1 1 1]', 2, @(x) 1);
    assertExceptionThrown(F, '');
    
    F = @() quadrature3D([0 1 0]', [1 0 0]', [0 0 0]', [1 1 1]', 3, @(x) 1);
    assertExceptionThrown(F, '');
    
    F = @() quadrature3D([0 1 0]', [1 0 0]', [0 0 0]', [1 1 1]', 6, @(x) 1);
    assertExceptionThrown(F, '');
end