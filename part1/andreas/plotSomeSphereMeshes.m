% Generates plots for problem 4b
% A sphere mesh

addpath('grids');
close all

% Generate a disk mesh
%   - N        the number of nodes in the mesh
%   - p        nodal points. (x,y)-coordinates for point i given in row i.
%   - tetra    elements. Index to the three corners of element i given in row i.
%   - surface  surface triangles. Three indexes to points per row

N = 1000;
[p tetra surface] = getSphere(N);


% Plot the sphere
tetramesh(tetra, p, 'FaceColor','red','EdgeColor','black');

% Pretty lighting
camlight 
lighting gouraud