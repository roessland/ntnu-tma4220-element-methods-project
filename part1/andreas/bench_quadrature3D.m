runtests test_quadrature3D
f = @(x) x(1) + x(2) + x(3);
tic
for i = 1:12500
    P = rand(3, 4);
    quadrature3D(P(:,1), P(:,2), P(:,3), P(:,4), 5, f);
end
toc