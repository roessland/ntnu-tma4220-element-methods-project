clear all
% Poisson solver in 2D
% See
% http://www.math.ntnu.no/emner/TMA4220/2013h/notes/TMA4220_2007-Lecture_Note_4.pdf
% for details.

% Variables
%   - Nn    the number of nodes in the mesh
%   - f     a function from R^2 to R
Nn = 20000;
f = @(x) -8*pi*cos(2*pi*(x(1)^2 + x(2)^2)) + 16*pi^2*(x(1)^2+x(2)^2)*sin(2*pi*(x(1)^2+x(2)^2));



% Ensure the getDisk() function is in path, and close exisiting figures
addpath('grids');
close all

% Generate a disk mesh

%   - p     nodal points. (x,y)-coordinates for point i given in row i.
%   - tri   elements. Index to the three corners of element i given in row i.
%   - edge  edge lines. Index list to the two corners of edge line i given in row i
[p tri edge] = getDisk(Nn);

% Build stiffness matrix Ah and load vector Fh
% Look trough all elements, and add contributions to the nodes.
%   - K    the number of elements in the mesh
K = size(tri,1);
Ah = zeros(Nn,Nn);
Fh = zeros(Nn,1);

% Unit vectors. Used for constructing the basis function planes on an
% element.
e = [1 0 0;
     0 1 0;
     0 0 1];

for k = 1:K
    % The corner numbers for this element
    trik = tri(k,:);
    
    % The corner positions for this element
    pk = [p(trik(1),:);
          p(trik(2),:);
          p(trik(3),:)];
      
    % The area of this element with the usual determinant formula.
    area = 1/2*abs(det([pk(2,:)-pk(1,:);
                        pk(3,:)-pk(1,:)]));
      
    % The matrix used to solve for the coefficients of the basis functions
    % on element k. (They are a plane)
    Mk = [[1;1;1] pk];
    
    % Find all the basis functions on this element. The alpha'th column has
    % the coefficients for the plane for corner alpha.
    % Phi on the alpha'th corner of element k is given by the plane
    % Hk(alpha) = ckalpha(1) + ckalpha(2)*x + ckalpha(3)*y. x, y and Hk is known at
    % the corners, and thus we can solve for ck.
    ck = zeros(3,3);
    for alpha = 1:3
        ck(:,alpha) = Mk\e(:,alpha);
    end
    
    % Ak is the elemental 3x3 matrix for this element, as given at page 13
    % in Lecture note 4.
    Ak = @(alpha, beta) area * (ck(2,alpha)*ck(2,beta) + ck(3,alpha)*ck(3,beta));
    
    % The procedure for constructing Ah works by iterating over all
    % elements, and for each element constructing all the 9 possible
    % integrals of the basis functions. We have combinations 11,22,33,12,13
    % etc. These integrals then contribute to the corner points of the
    % element.
    for alpha = 1:3
        i = trik(alpha);
        
        % First, construct the load vector component using quadrature to
        % evaluate the integral. Hk is the plane over this element, defined
        % so that Hk(p1) = 1, Hk(p2) = 0, Hk(p3) = 0.
        
        % Fkalpha = integral of f * Hkalpha over the element
        Hk = @(x) ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2);        
        g = @(x) f(x) * Hk(x);
        Fkalpha = quadrature2D(pk(1,:)', pk(2,:)', pk(3,:)', 4, g);
        Fh(i) = Fh(i) + Fkalpha;
        
        
        % The remaining part is for Ah
        for beta = 1:3
            j = trik(beta);
            Ah(i,j) = Ah(i,j) + Ak(alpha, beta);
        end
        
        
    end
end

% The system is now singular, and cannot be solved. This is because we have
% too many basis functions. To fix this, we will remove all boundary
% points, because they are zero anyways.

% Get the inner nodes, by subtracting the set of edge nodes from the set of
% all nodes.
all_nodes = 1:Nn;
boundary_nodes = unique(edge);
inner_nodes = setdiff(all_nodes, boundary_nodes);

% Select only the inner parts of everything
Ah_inner = Ah(inner_nodes, inner_nodes);
Fh_inner = Fh(inner_nodes);
tri_inner = tri(inner_nodes,:);

% Solve the inner part
uh_inner = Ah_inner \ Fh_inner;

% Build z-coordinates vector by setting all points to zero, and then
% rebuilding the inner valus.
z = zeros(Nn,1);
for i = 1:length(uh_inner)
    z(inner_nodes(i)) = uh_inner(i);
end
 
% Plot solution
close all
figure
trimesh(tri, p(:,1), p(:,2), z);

% Plot error
err = zeros(Nn, 1);
for i = 1:Nn
   pn = p(i,:);
   err(i) = z(i) - sin(2*pi*(pn(1)^2 + pn(2)^2));
end
figure
plot(err)
legend('Error');




