function test_suite = test_quadrature2D
    initTestSuite
end


% Test that the integral is zero if the triangle is degenerate
function testZeroAreaTriangle
    g = @(x) 1000;
    p1 = [1 1]'; p2 = [2 2]'; p3 = [3 3]';
    
    Ia = 0;
    
    Ib = quadrature2D(p1, p2, p3, 1, g);
    assertEqual(Ia, Ib);
    
    Ib = quadrature2D(p1, p2, p3, 3, g);
    assertEqual(Ia, Ib);
    
    Ib = quadrature2D(p1, p2, p3, 4, g);
    assertEqual(Ia, Ib);
end

% Test that the integral of 1 is equal to the area of the triangle
function testAreaEqualsIntegralOfOne
    g = @(x) 1;
    p1 = [1 0]'; p2 = [0 1]'; p3 = [0 0]';
    
    Ia = 1/2;
    
    Ib = quadrature2D(p1, p2, p3, 1, g);
    assertEqual(Ia, Ib);
    
    Ib = quadrature2D(p1, p2, p3, 3, g);
    assertEqual(Ia, Ib);
    
    Ib = quadrature2D(p1, p2, p3, 4, g);
    assertEqual(Ia, Ib);
end

% Test that integral of log(x + y) is correct
function testLogIntegral
    g = @(x) log(x(1) + x(2));
    p1 = [1 0]'; p2 = [3 1]'; p3 = [3 2]';
    
    Ia =  1.16542;
    
    Ib = quadrature2D(p1, p2, p3, 1, g);
    assertElementsAlmostEqual(Ia, Ib, 'absolute', 0.1);
    
    Ib = quadrature2D(p1, p2, p3, 3, g);
    assertElementsAlmostEqual(Ia, Ib, 'absolute', 0.01);
    
    Ib = quadrature2D(p1, p2, p3, 4, g);
    assertElementsAlmostEqual(Ia, Ib, 'absolute', 0.01);
end

% Test that illegal point rules throw an exception
function testExceptionThrownWhenIllegalPointRule
    F = @() quadrature2D([0 1]', [1 0]', [0 0]', 5, @(x) 1);
    assertExceptionThrown(F, '');
end