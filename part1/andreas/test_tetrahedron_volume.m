function test_suite = test_tetrahedron_volume
    initTestSuite
end


% Test that the volume is zero if the tetrahedron is degenerate,
% that is, if three or more points are on the same line.
function testZeroVolumeTetrahedron
    p1 = [1 1 1]'; p2 = [2 2 2]'; p3 = [3 3 3]'; p4 = [1 10 -3]';

    assertEqual(tetrahedron_volume(p1,p2,p3,p4), 0);
end

% Test the volume of a simple pyramid
function testSimplePyramid
    p1 = [0 0 1]'; p2 = [0 1 0]'; p3 = [1 0 0]'; p4 = [0 0 0]';
    
    % This is a pyramid. Area of the base * height * 1/3;
    actual_volume = 1/2 * 1 * 1/3;
    
    assertEqual(actual_volume, tetrahedron_volume(p1,p2,p3,p4));
end