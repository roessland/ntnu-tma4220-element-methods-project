function test_suite = test_quadrature1D
    initTestSuite
end

% Check that integral of 1000 from x=5 to x=5 is zero
function testZeroInterval
    a = 5;
    b = 5;
    actual_integral = 0;
    g = @(x) 1000;

    % One quadrature point
    quadrature_integral = quadrature1D(a, b, 1, g);
    assertEqual(actual_integral, quadrature_integral);
    
    % Two quadrature points
    quadrature_integral = quadrature1D(a, b, 1, g);
    assertEqual(actual_integral, quadrature_integral);
    
    % Three quadrature points
    quadrature_integral = quadrature1D(a, b, 1, g);
    assertEqual(actual_integral, quadrature_integral);
    
    % Four quadrature points
    quadrature_integral = quadrature1D(a, b, 1, g);
    assertEqual(actual_integral, quadrature_integral);
end

% Check that integral of e^x from 1 to 2 is approximately equal to
% quadrature solution.
function testExponentialFunction
    a = 1;
    b = 2;
    actual_integral = exp(b) - exp(a);
    
    % One quadrature point
    quadrature_integral = quadrature1D(a, b, 1, @exp);
    assertElementsAlmostEqual(actual_integral, quadrature_integral, 'relative', 0.05);
    
    % Two quadrature points
    quadrature_integral = quadrature1D(a, b, 2, @exp);
    assertElementsAlmostEqual(actual_integral, quadrature_integral, 'relative', 0.005);
    
    % Three quadrature points
    quadrature_integral = quadrature1D(a, b, 3, @exp);
    assertElementsAlmostEqual(actual_integral, quadrature_integral, 'relative', 0.0001);
    
    % Four quadrature points
    quadrature_integral = quadrature1D(a, b, 4, @exp);
    assertElementsAlmostEqual(actual_integral, quadrature_integral, 'relative', 0.0001);
end

% Check that it can handle a simple line integral
function testConstantLineIntegral
    a = [0; 0];
    b = [1; 0];
    g = @(x) 1;
    actual_integral = 1;
    
    quadrature_integral = quadrature1D(a, b, 1, g);
    assertEqual(actual_integral, quadrature_integral);
    
    quadrature_integral = quadrature1D(a, b, 2, g);
    assertEqual(actual_integral, quadrature_integral);
    
    quadrature_integral = quadrature1D(a, b, 3, g);
    assertEqual(actual_integral, quadrature_integral);
    
    quadrature_integral = quadrature1D(a, b, 4, g);
    assertEqual(actual_integral, quadrature_integral);
end

% Try with a linear line integral
% http://mathinsight.org/line_integral_scalar_examples
function testLinearLineIntegral
    a = [1;2];
    b = [4;3];
    g = @(x) x(1) + x(2);
    actual_integral = 5*sqrt(10);
    
    quadrature_integral = quadrature1D(a, b, 1, g);
    assertEqual(actual_integral, quadrature_integral);
end








