function test_suite = test_norm
    initTestSuite
end


% Test that the norms are euclidean by default
function testZeroVolumeTetrahedron
    p = [1 1 1]';
    assertElementsAlmostEqual(norm(p)^2, 3, 'absolute', 0.00001);
    
    p = [1 0 1]';
    assertElementsAlmostEqual(norm(p)^2, 2, 'absolute', 0.00001);
end