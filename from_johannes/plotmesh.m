function  plotmesh
close all;
[p tri edge]=getDisk(10);
triplot(tri,p(:,1),p(:,2));
for i=1:10
   text(p(i,1),p(i,2),int2str(i)); 
end
figure 
[p tri edge]=getDisk(20);
triplot(tri,p(:,1),p(:,2));
for i=1:20
   text(p(i,1),p(i,2),int2str(i)); 
end
figure 
[p tri edge]=getDisk(100);
triplot(tri,p(:,1),p(:,2));
for i=1:100
   text(p(i,1),p(i,2),int2str(i)); 
end
end

