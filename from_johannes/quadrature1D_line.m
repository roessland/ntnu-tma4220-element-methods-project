function I = quadrature1D_line(a,b,Nq,g )
% approximates the line integral of g between a and b with gauss quadrature
%Used in Part 1 Ex 3c

% I  value of the integral
% a  start point
% b integration end point
% Nq  [1, 4] number of integration points
%g  function pointer
z=zeros(Nq,1);
p=zeros(Nq,1);
if Nq==1
    z(1)=1/2;
    p(1)=1;
elseif Nq==2
    z(1)=(((-sqrt(1/3))+1)/2);
    z(2)=(((sqrt(1/3))+1)/2);
    p(1)=1/2;
    p(2)=1/2;
elseif Nq==3
    z(1)=(((-sqrt(3/5))+1)/2);
    z(2)=(((0)+1)/2);
    z(3)=(((sqrt(3/5))+1)/2);
    p(1)=1/2*5/9;
    p(2)=1/2*8/9;
    p(3)=1/2*5/9;
elseif Nq==4
    z(1)=(((-sqrt((3+2*sqrt(6/5))/7))+1)/2);
    z(2)=(((-sqrt((3-2*sqrt(6/5))/7))+1)/2);
    z(3)=(((sqrt((3-2*sqrt(6/5))/7))+1)/2);
    z(4)=(((sqrt((3+2*sqrt(6/5))/7))+1)/2);
    p(1)=1/2*(18-sqrt(30))/36;
    p(2)=1/2*(18+sqrt(30))/36;
    p(3)=1/2*(18+sqrt(30))/36;
    p(4)=1/2*(18-sqrt(30))/36;
    
end
    

I=0;
for i=1:Nq
    I=I+p(i)*g(a(1)+z(i)*(b(1)-a(1)),a(2)+z(i)*(b(2)-a(2)))*norm(b-a);
end


end

