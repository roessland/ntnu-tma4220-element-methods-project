function f = createf( n )

% M #triangles
% n #nodes
[p tri edge]=getDisk(n); %create triangle mash
M=size(tri,1);   %number of triangles
%calculate c
c=zeros(3,3,M);
E=eye(3,3);
for k=1:M
    for alpha =1:3
        Ma=[1 p(tri(k,1),1) p(tri(k,1),2);1 p(tri(k,2),1) p(tri(k,2),2);1 p(tri(k,3),1) p(tri(k,3),2)];
        C=Ma\E(:,alpha);
        c(:,alpha,k)=C;
%                if (p(tri(k,alpha),1)^2 +p(tri(k,alpha),2)^2)==1  %boundary conditions
%                    c(:,alpha,k)=zeros(3,1);
%                end
    end
end
H=@(x,y,k,alpha) c(1,alpha,k)+c(2,alpha,k)*x+c(3,alpha,k)*y;
f_=@(x,y) -8*pi*cos(2*pi*(x^2+y^2))+16*pi^2*(x^2+y^2)*sin(2*pi*(x^2+y^2));

F=zeros(3,M);

for k=1:M
    for alpha=1:3
        F_=@(x,y) f_(x,y)*H(x,y,k,alpha);
        F(alpha,k) =quadrature2D(p(tri(k,1),:),p(tri(k,2),:),p(tri(k,3),:),4,F_);
        
%         if k==1 && alpha==1
%             
%             z1=zeros(10,10);
%             z2=zeros(10,10);
%             z3=zeros(10,10);
%             for x=1:100
%                 for y=1:100
% %                     z1(x,y)=f_((x-50)/50,(y-50)/50);
% %                     z2(x,y)=H((x-50)/50,(y-50)/50,k,alpha);
%                     z3(x,y)=F_((x-50)/50,(y-50)/50);
%                 end
%             end
% %             x=-49/50:0.02:1;
% %             y=-49/50:0.02:1;
% %             
% %             close all
% %             surf(x,y,z1);
% %             figure
% %             surf(x,y,z2);
% % %             figure
% %             surf(x,y,z3);
% %             p(tri(k,1),:)
% %             p(tri(k,2),:)
% %             p(tri(k,3),:)
% %             F_(p(tri(k,1),1),p(tri(k,1),2))
% %             F_(p(tri(k,2),1),p(tri(k,2),2))
% %             F_(p(tri(k,3),1),p(tri(k,3),2))
%         end
    end
    
end
f=zeros(n,1);
for k=1:M  %cmp lecture: Implementation
    for alpha=1:3
        i=tri(k,alpha);
        
        
        f(i)= f(i)+F(alpha,k);
        
        
    end
    
end
end



