function plotresult_3(n)
close all;
x=1:n;
y=zeros(n,1);

A=CreateA(n);
f=createf_n(n);
[p  tri ed]=getDisk(n);
del=[]; 
ex=[];
for i=1:size(p,1)
    if ((p(i,1)^2+p(i,2)^2)>1-1/n)&&(p(i,2)<0)
       del=[del,i]; 
    else
        ex=[ex,i];
    end
end

u=zeros(size(p,1),1);



u(ex)=A(ex,ex)\f(ex);
u(del)=0;


xx=zeros(n,1);
yy=zeros(n,1);
zz=zeros(n,1);
zzz=zeros(n,1);
for i=1:n;
    y(i)=sin(2*pi*(p(i,1)^2+p(i,2)^2))-u(i);
    xx(i)=p(i,1);
    yy(i)=p(i,2);
    zz(i)=sin(2*pi*(p(i,1)^2+p(i,2)^2));
    %[p(i,1) p(i,2) sin(2*pi*(p(i,1)^2+p(i,2)^2)) u(i)]
    
    zzz(i)=u(i);
end

plot(x,y);
figure
trimesh(tri,xx,yy,zz);
figure
trimesh(tri,xx,yy,zzz);

end

