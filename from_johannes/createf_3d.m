function f = createf_3d( n )

% M #triangles
% n #nodes
[p tri edge]=getSphere(n); %create triangle mash
M=size(tri,1);   %number of triangles
%calculate c

c=zeros(4,4,M);
E=eye(4,4);
for k=1:M
   for alpha =1:4
      Ma=[1 p(tri(k,1),1) p(tri(k,1),2) p(tri(k,1),3);1 p(tri(k,2),1) p(tri(k,2),2) p(tri(k,2),3);1 p(tri(k,3),1) p(tri(k,3),2) p(tri(k,3),3);1 p(tri(k,4),1) p(tri(k,4),2) p(tri(k,4),3)];
      C=Ma\E(:,alpha);
      c(:,alpha,k)=C;
%        if (p(tri(k,alpha),1)^2 +p(tri(k,alpha),2)^2)==1  %boundary conditions
%            c(:,alpha,k)=zeros(3,1);
%        end
   end
end

H=@(x,y,z,k,alpha) c(1,alpha,k)+c(2,alpha,k)*x+c(3,alpha,k)*y+c(4,alpha,k)*z;
f_=@(x,y,z) -12*pi*cos(2*pi*(x^2+y^2+z^2))+16*pi^2*(x^2+y^2+z^2)*sin(2*pi*(x^2+y^2+z^2));

F=zeros(4,M);

for k=1:M
    for alpha=1:4
        F_=@(x,y,z) f_(x,y,z)*H(x,y,z,k,alpha);
        F(alpha,k) =quadrature3D(p(tri(k,1),:),p(tri(k,2),:),p(tri(k,3),:),p(tri(k,4),:),5,F_);
        

    end
    
end
f=zeros(n,1);
for k=1:M  %cmp lecture: Implementation
    for alpha=1:4
        i=tri(k,alpha);
        
        
        f(i)= f(i)+F(alpha,k);
        
        
    end
    
end
end



