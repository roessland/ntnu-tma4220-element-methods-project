function plotresult_3d_n(n)
close all;
x=1:n;
y=zeros(n,1);

A=CreateA_3d(n);
f=createf_3d_n(n);
[p  tri ed]=getSphere(n);
del=[]; 
ex=[];
for i=1:size(p,1)
    if ((p(i,1)^2+p(i,2)^2+p(i,3)^2)>1-1/n)&&(p(i,2)<0)
       del=[del,i]; 
    else
        ex=[ex,i];
    end
end

u=zeros(size(p,1),1);

u(ex)=A(ex,ex)\f(ex);
u(del)=0;

close all;
tetramesh(tri,p,'FaceColor','red','EdgeColor','black');
camlight 
lighting gouraud

%u
F = TriScatteredInterp(p, u);
figure
%F(1,1,1)
x=0:0.025:1;
y=-1:0.05:1;
z=-1:0.05:1;
v=zeros(41,41,41);
for i=x
    for j=y
        for k=z
           
            v(round(i*40+1),round(j*20+21),round(k*20+21))=F(i,j,k);
        end
    end
end
cmap=colormap(hot);

for i=-0.9:0.2:0.9
    
p = patch(isosurface(x,y,z,v,i));
isonormals(x,y,z,v,p)

set(p,'FaceColor',cmap(round(((i+1)/2)*64+1),:),'EdgeColor','none');
camlight 
lighting gouraud
hold on
end

hold off
end
