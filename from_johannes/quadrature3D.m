function  I = quadrature3D(p1,p2,p3,p4,Nq,g)
% I value of the integral
% p1 first corner point of the triangle
% p2 second corner point of the triangle
% p3 third corner point of the triangle
% p4 fourth corner point of the triangle
% Nq in [1; 4; 5] number of integration points
% g function pointer

A=1/6*abs(det([p2-p1;p3-p1;p4-p1])); %volume of A

zeta=zeros(Nq,4);
rho=zeros(Nq,1);

if Nq ==1
    zeta=[1/4, 1/4, 1/4, 1/4];
    rho(1)=1;
elseif Nq ==4
    zeta=[0.5854102, 0.1381966, 0.1381966, 0.1381966;0.1381966, 0.5854102, 0.1381966, 0.1381966;0.1381966, 0.1381966, 0.5854102, 0.1381966;0.1381966, 0.1381966, 0.1381966, 0.5854102];
    rho(1)=1/4;
    rho(2)=1/4;
    rho(3)=1/4;
    rho(4)=1/4;
elseif Nq ==5  
    zeta=[1/4,1/4,1/4,1/4;1/2,1/6,1/6,1/6;1/6,1/2,1/6,1/6;1/6,1/6,1/2,1/6;1/6,1/6,1/6,1/2];
    rho(1)=-4/5;
    rho(2)=9/20;
    rho(3)=9/20;
    rho(4)=9/20;
    rho(5)=9/20;
else
    'wrong Nq'
end

z=zeta*[p1;p2;p3;p4];

rho=rho*A;
I=0;
for i=1:Nq
    I=I+rho(i)*g(z(i,1),z(i,2),z(i,3));
end
    

end

