function I = quadrature1D(a,b,Nq,g )
% I  value of the integral
% a  integration start
% b integration end
% Nq  [1, 4] number of integration points
%g  function pointer
z=zeros(Nq,1);
p=zeros(Nq,1);
if Nq==1
    z(1)=(b+a)/2;
    p(1)=(b-a);
elseif Nq==2
    z(1)=(((-sqrt(1/3))+1)/2)*(b-a)+a;
    z(2)=(((sqrt(1/3))+1)/2)*(b-a)+a;
    p(1)=(b-a)/2;
    p(2)=(b-a)/2;
elseif Nq==3
    z(1)=(((-sqrt(3/5))+1)/2)*(b-a)+a;
    z(2)=(((0)+1)/2)*(b-a)+a;
    z(3)=(((sqrt(3/5))+1)/2)*(b-a)+a;
    p(1)=(b-a)/2*5/9;
    p(2)=(b-a)/2*8/9;
    p(3)=(b-a)/2*5/9;
elseif Nq==4
    z(1)=(((-sqrt((3+2*sqrt(6/5))/7))+1)/2)*(b-a)+a;
    z(2)=(((-sqrt((3-2*sqrt(6/5))/7))+1)/2)*(b-a)+a;
    z(3)=(((sqrt((3-2*sqrt(6/5))/7))+1)/2)*(b-a)+a;
    z(4)=(((sqrt((3+2*sqrt(6/5))/7))+1)/2)*(b-a)+a;
    p(1)=(b-a)/2*(18-sqrt(30))/36;
    p(2)=(b-a)/2*(18+sqrt(30))/36;
    p(3)=(b-a)/2*(18+sqrt(30))/36;
    p(4)=(b-a)/2*(18-sqrt(30))/36;
    
end
    

I=0;
for i=1:Nq
    I=I+p(i)*g(z(i));
end


end

