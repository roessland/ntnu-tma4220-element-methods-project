function A_ = CreateA( n )
% M #triangles
% n #nodes

%addpath(genpath(pwd))


[p tri edge]=getDisk(n); %create triangle mash
M=size(tri,1);   %number of triangles
%calculate c
c=zeros(3,3,M);
E=eye(3,3);
for k=1:M
   for alpha =1:3
      Ma=[1 p(tri(k,1),1) p(tri(k,1),2);1 p(tri(k,2),1) p(tri(k,2),2);1 p(tri(k,3),1) p(tri(k,3),2)];
      C=Ma\E(:,alpha);
      c(:,alpha,k)=C;
%        if (p(tri(k,alpha),1)^2 +p(tri(k,alpha),2)^2)==1  %boundary conditions
%            c(:,alpha,k)=zeros(3,1);
%        end
   end
end

%c
A=zeros(3,3,M);
area=zeros(M,1);
 for k=1:M
  
    area(k)=1/2*abs(det([p(tri(k,2),:)-p(tri(k,1),:);p(tri(k,3),:)-p(tri(k,1),:)])); %area of each triangel 
    for alpha=1:3
        for beta_=1:3
            A(alpha,beta_,k)=area(k)*(c(2,alpha,k)*c(2,beta_,k)+c(3,alpha,k)*c(3,beta_,k));
         
        end
        
    end
    
end
A_=zeros(n,n);
for k=1:M  %cmp lecture: Implementation 
    for alpha=1:3
        i=tri(k,alpha);
        for beta_=1:3
            j=tri(k,beta_);
            
            A_(i,j)= A_(i,j)+A(alpha,beta_,k);
        end
        
    end
    
end
end

