function A_ = CreateA_3d( n )
% M #triangles
% n #nodes

%addpath(genpath(pwd))


[p tri edge]=getSphere(n); %create tetradeder mash
M=size(tri,1);   %number of tetraeders
%calculate c
c=zeros(4,4,M);
E=eye(4,4);
for k=1:M
   for alpha =1:4
      Ma=[1 p(tri(k,1),1) p(tri(k,1),2) p(tri(k,1),3);
          1 p(tri(k,2),1) p(tri(k,2),2) p(tri(k,2),3);
          1 p(tri(k,3),1) p(tri(k,3),2) p(tri(k,3),3);
          1 p(tri(k,4),1) p(tri(k,4),2) p(tri(k,4),3)];
      C=Ma\E(:,alpha);
      c(:,alpha,k)=C;
%        if (p(tri(k,alpha),1)^2 +p(tri(k,alpha),2)^2)==1  %boundary conditions
%            c(:,alpha,k)=zeros(3,1);
%        end
   end
end

%c
A=zeros(4,4,M);
area=zeros(M,1);
 for k=1:M
    %volume of each tetraeder
    area(k)=1/6*abs(det([p(tri(k,2),:)-p(tri(k,1),:);
            p(tri(k,3),:)-p(tri(k,1),:);p(tri(k,4),:)-p(tri(k,1),:)]));
    for alpha=1:4
        for beta_=1:4
            A(alpha,beta_,k)=area(k)*(c(2,alpha,k)*c(2,beta_,k)+...
              c(3,alpha,k)*c(3,beta_,k)+c(4,alpha,k)*c(4,beta_,k));
         
        end
        
    end
    
end
A_=zeros(n,n);
for k=1:M  %cmp lecture: Implementation 
    for alpha=1:4
        i=tri(k,alpha);
        for beta_=1:4
            j=tri(k,beta_);
            
            A_(i,j)= A_(i,j)+A(alpha,beta_,k);
        end
        
    end
    
end
end

