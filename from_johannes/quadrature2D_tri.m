function I = quadrature2D_tri(p1,p2,p3,Nq,g)
% I value of the integral
% p1 first corner point of the triangle
% p2 second corner point of the triangle
% p3 third corner point of the triangle
% Nq in [1; 3; 4] number of integration points
% g function pointer

A=(1/2)*norm(cross(p2-p1,p3-p1)); %area of A (triangel in the 3D space)


zeta=zeros(Nq,3);
rho=zeros(Nq,1);

if Nq ==1
    zeta=[1/3,1/3,1/3];
    rho(1)=1;
elseif Nq ==3
    zeta=[1/2,1/2,0;1/2,0,1/2;0,1/2,1/2];
    rho(1)=1/3;
    rho(2)=1/3;
    rho(3)=1/3;
elseif Nq ==4  
    zeta=[1/3,1/3,1/3;3/5,1/5,1/5;1/5,3/5,1/5;1/5,1/5,3/5];
    rho(1)=-9/16;
    rho(2)=25/48;
    rho(3)=25/48;
    rho(4)=25/48;
else
    'wrong Nq'
end

z=zeta*[p1;p2;p3];

rho=rho*A;
I=0;
for i=1:Nq
    I=I+rho(i)*g(z(i,1),z(i,2),z(i,3));
    
end
    

end

