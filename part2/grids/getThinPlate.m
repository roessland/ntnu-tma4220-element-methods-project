function [p tri edge] = getThinPlate(N)
% function [p tri edge] = getPlate(N),
% 
% description:
%      generate a mesh triangulation of a thin plate in (-1,1)^3
%
% arguments:
%   - N    the number of nodes in each spatial direction (2*N^2 total nodes)
% returns:
%   - p     nodal points. (x,y)-coordinates for point i given in row i.
%   - tri   elements. Index to the three corners of element i given in row i.
%   - edge  index list of all nodal points on the outer edge



p = zeros(N^2*2,3);
edge = [];

k = 1;
for j=1:N,
	for i=1:N,
        for l=1:2
            p(k,:) = [(i-1)/(N-1), (j-1)/(N-1), (l-1)/(N-1)] * 2 - 1;

            if j == 1 || j == N || i == 1 || i == N
               edge = [edge k]; 
            end
            
            k = k+1;
        end
	end
end

tri  = delaunay(p(:,1), p(:,2),p(:,3));

end
