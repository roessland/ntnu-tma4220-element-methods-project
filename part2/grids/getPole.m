function [p tri edge] = getPole(N),
% function [p tri edge] = getPlate(N),
% 
% description:
%      generate a pole of height around N/4
%
% arguments:
%   - N    the number of nodes
% returns:
%   - p     nodal points. (x,y)-coordinates for point i given in row i.
%   - tri   elements. Index to the three corners of element i given in row i.
%   - edge  index list of all nodal points on the boundary

% author: ruben
% last edit: november 2013

if mod(N,4)>0 || N<8
    error('must have at least 8 points, and a multiple of 4');
end

p=zeros(N,3);

% points
ix=1;
for i=1:(N/4)
    p(ix,1)=0; p(ix,2)=1; p(ix,3)=N/4-i; ix=ix+1;
    p(ix,1)=1; p(ix,2)=1; p(ix,3)=N/4-i; ix=ix+1;
    p(ix,1)=1; p(ix,2)=0; p(ix,3)=N/4-i; ix=ix+1;
    p(ix,1)=0; p(ix,2)=0; p(ix,3)=N/4-i; ix=ix+1;
end

% just the top
edge=[1 2 3 4];

% triangulation vrael
tri=zeros(N/4-1,4);
ix=1;
for i=1:N/4-1
    j=i*4-4;
    tri(ix,1)=1+j; tri(ix,2)=2+j; tri(ix,3)=4+j; tri(ix,4)=8+j; ix=ix+1;
    tri(ix,1)=1+j; tri(ix,2)=2+j; tri(ix,3)=5+j; tri(ix,4)=8+j; ix=ix+1;
    tri(ix,1)=2+j; tri(ix,2)=3+j; tri(ix,3)=4+j; tri(ix,4)=8+j; ix=ix+1;
    tri(ix,1)=2+j; tri(ix,2)=3+j; tri(ix,3)=7+j; tri(ix,4)=8+j; ix=ix+1;
    tri(ix,1)=2+j; tri(ix,2)=5+j; tri(ix,3)=6+j; tri(ix,4)=8+j; ix=ix+1;
    tri(ix,1)=2+j; tri(ix,2)=6+j; tri(ix,3)=7+j; tri(ix,4)=8+j; ix=ix+1;
    % mirror every second element
    if mod(i,2)==1
        for k=ix-6:ix-1
            for j=1:4
                if mod(tri(k,j),2)==0
                    tri(k,j)=tri(k,j)-1;
                else
                    tri(k,j)=tri(k,j)+1;
                end
            end
        end
    end
end
