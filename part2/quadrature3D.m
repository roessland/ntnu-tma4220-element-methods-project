function [ I ] = quadrature3D( p1, p2, p3, p4, Nq, g )
%QUADRATURE3D Integrates a function over a tetrahedron using quadrature

%
%   I \in R          value of the integral
%   p1 \in R^3       first corner point of the triangle  (vertical matrix)
%   p2 \in R^3       second corner point of the triangle (vertical matrix)
%   p3 \in R^3       third corner point of the triangle  (vertical matrix)
%   p4 \in R^3       third corner point of the triangle  (vertical matrix)
%   Nq \in {1,4,5}   number of integration points
%   g: R^3 \to R     function pointer (must take vertical matrix)
%


% Set quadrature parameters
%
% wq = weights
% zq = quadrature points in barycentric coordinates

if Nq == 1
    % 1-point rule
    wq = [1];
    Zq = [1/4 1/4 1/4 1/4];
elseif Nq == 4
    % 4-point rule
    wq = [0.25 0.25 0.25 0.25]';
      
    Zq = [0.5854102 0.1381966 0.1381966 0.1381966;
          0.1381966 0.5854102 0.1381966 0.1381966;
          0.1381966 0.1381966 0.5854102 0.1381966;
          0.1381966 0.1381966 0.1381966 0.5854102];   
elseif Nq == 5
    % 5-point rule
    wq = [-4/5; 9/20; 9/20; 9/20; 9/20];
      
    Zq = [1/4 1/4 1/4 1/4;
          1/2 1/6 1/6 1/6;
          1/6 1/2 1/6 1/6;
          1/6 1/6 1/2 1/6;
          1/6 1/6 1/6 1/2];
else
    error('Only the 1, 4 and 5 point rules are supported');
end

% Transform quadrature points from barycentric coordinates to physical
% coordinates, using x = z1 p1 + z2 p2 + z3 p3.
P = [p1 p2 p3 p4];
Xq = P * Zq';

% Compute quadrature
I = 0;
for n = 1:Nq
    I = I + wq(n) * g(Xq(:,n));
end

% This was for a tetrahedron with volume 1, so we have to multiply with the
% volume of the real triangle.
% See http://en.wikipedia.org/wiki/Tetrahedron#Volume
V = tetrahedron_volume(p1, p2, p3, p4);
I = I * V;

end

