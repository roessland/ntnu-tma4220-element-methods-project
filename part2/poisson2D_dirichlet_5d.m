function poisson2D_dirichlet_5d(Nn)
% Poisson solver in 2D
% See
% http://www.math.ntnu.no/emner/TMA4220/2013h/notes/TMA4220_2007-Lecture_Note_4.pdf
% for details.

% Variables
%   - Nn    the number of nodes in the mesh
%   - f     a function from R^2 to R
E=1;
nu=0.5;
%f = @(x) -8*pi*cos(2*pi*(x(1)^2 + x(2)^2)) + 16*pi^2*(x(1)^2+x(2)^2)*sin(2*pi*(x(1)^2+x(2)^2));
%f=@(x) E/(1-nu^2)*[-2*x(2)^2-x(1)^2+nu*x(1)^2-2*nu*x(1)*x(2)-2*x(1)*x(2)+3-nu;...
%    -2*x(1)^2-x(2)^2+nu*x(2)^2-2*nu*x(2)*x(1)-2*x(1)*x(2)+3-nu];
f1=@(x) E/(1-nu^2)*(-2*x(2)^2-x(1)^2+nu*x(1)^2-2*nu*x(1)*x(2)-2*x(1)*x(2)+3-nu);
f2=@(x) E/(1-nu^2)*(-2*x(1)^2-x(2)^2+nu*x(2)^2-2*nu*x(2)*x(1)-2*x(1)*x(2)+3-nu);


% Ensure the getDisk() function is in path, and close exisiting figures
addpath('grids');
close all

% Generate a disk mesh

%   - p     nodal points. (x,y)-coordinates for point i given in row i.
%   - tri   elements. Index to the three corners of element i given in row i.
%   - edge  edge lines. Index list to the two corners of edge line i given in row i
[p tri edge] = getPlate(Nn);

% Build stiffness matrix Ah and load vector Fh
% Look trough all elements, and add contributions to the nodes.
%   - K    the number of elements in the mesh
K = size(tri,1);
Ah = zeros(2*Nn^2,2*Nn^2);
Fh = zeros(2*Nn^2,1);

% Unit vectors. Used for constructing the basis function planes on an
% element.
e = eye(3);
    %5d

for k = 1:K
    % The corner numbers for this element
    trik = tri(k,:);
    
    % The corner positions for this element
    pk = [p(trik(1),:);
          p(trik(2),:);
          p(trik(3),:)];
      
    % The area of this element with the usual determinant formula.
    area = 1/2*abs(det([pk(2,:)-pk(1,:);
                        pk(3,:)-pk(1,:)]));
      
    % The matrix used to solve for the coefficients of the basis functions
    % on element k. (They are a plane)
    Mk = [[1;1;1] pk];
        %5d
    
    % Find all the basis functions on this element. The alpha'th column has
    % the coefficients for the plane for corner alpha.
    % Phi on the alpha'th corner of element k is given by the plane
    % Hk(alpha) = ckalpha(1) + ckalpha(2)*x + ckalpha(3)*y. x, y and Hk is known at
    % the corners, and thus we can solve for ck.
    ck = zeros(3,3);
    for alpha = 1:3
        
            ck(:,alpha) = (Mk\e(:,alpha));
        
    end
        %changed for 5d
    
    % Ak is the elemental 3x3 matrix for this element, as given at page 13
    % in Lecture note 4.
    
    Ak = @(alpha, beta) E/(1-nu^2) *area * [ck(2,alpha)*ck(2,beta)+((1-nu)/2)*ck(3,alpha)*ck(3,beta) ,...
        nu*(ck(2,alpha)*ck(3,beta))+((1-nu)/2)*ck(3,alpha)*ck(2,beta);...
        nu*(ck(3,alpha)*ck(2,beta))+((1-nu)/2)*ck(2,alpha)*ck(3,beta),...
        ck(3,alpha)*ck(3,beta)+((1-nu)/2)*ck(2,alpha)*ck(2,beta)];
        
        %(ck(2,alpha,gamma)*ck(2,beta,delta) + ck(2,alpha,gamma)*ck(6,beta,delta)+...
        % ck(6,alpha,gamma)*ck(2,beta,delta) + ck(6,alpha,gamma)*ck(6,beta,delta)+...
        %(1-nu)/2*(ck(3,alpha,gamma)*ck(3,beta,delta) + ck(3,alpha,gamma)*ck(5,beta,delta)+...
        %ck(5,alpha,gamma)*ck(3,beta,delta) + ck(5,alpha,gamma)*ck(5,beta,delta)));
    
    % The procedure for constructing Ah works by iterating over all
    % elements, and for each element constructing all the 9 possible
    % integrals of the basis functions. We have combinations 11,22,33,12,13
    % etc. These integrals then contribute to the corner points of the
    % element.
    for alpha = 1:3
        i= trik(alpha);
        %for gamma=1:2
            % First, construct the load vector component using quadrature to
            % evaluate the integral. Hk is the plane over this element, defined
            % so that Hk(p1) = 1, Hk(p2) = 0, Hk(p3) = 0.
            
            % Fkalpha = integral of f * Hkalpha over the element
             Hk = @(x) [ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2),0;0,ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2)];
             %g = @(x)  Hk(x)*f(x);
             g1=@(x) (ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2))*f1(x);
              g2=@(x) (ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2))*f2(x);
             Fkalpha = [quadrature2D(pk(1,:)', pk(2,:)', pk(3,:)', 4, g1),quadrature2D(pk(1,:)', pk(2,:)', pk(3,:)', 4, g2)];

%             Hk = @(x) ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2);
%             g = @(x) f(x) * Hk(x);
%             Fkalpha = quadrature2D(pk(1,:)', pk(2,:)', pk(3,:)', 4, g);
             Fh(2*i-1) = Fh(2*i-1) + Fkalpha(1);
             Fh(2*i) = Fh(2*i) + Fkalpha(2);
%             
            
            % The remaining part is for Ah
            for beta = 1:3
                %for delta=1:2
                     j= trik(beta);
                    
                    Ah(2*(i-1)+1:2*(i-1)+2,2*(j-1)+1:2*(j-1)+2) = Ah(2*(i-1)+1:2*(i-1)+2,2*(j-1)+1:2*(j-1)+2) + Ak(alpha, beta);
                %end
            end
        %end
        
        
    end
end
% The system is now singular, and cannot be solved. This is because we have
% too many basis functions. To fix this, we will remove all boundary
% points, because they are zero anyways.

% Get the inner nodes, by subtracting the set of edge nodes from the set of
% all nodes.
all_nodes = 1:Nn^2;
boundary_nodes = unique(edge);

inner_nodes_single = setdiff(all_nodes, boundary_nodes);
si=size(inner_nodes_single,2);
inner_nodes = zeros(1,2*si);

for i = 1:si
    inner_nodes(2*i-1)=2*inner_nodes_single(i)-1;
    inner_nodes(2*i)=2*inner_nodes_single(i);
end

% Select only the inner parts of everything
Ah_inner = Ah(inner_nodes, inner_nodes);
Fh_inner = Fh(inner_nodes);
%tri_inner = tri(inner_nodes,:);
% Solve the inner part
uh_inner = Ah_inner \ Fh_inner;

% Build z-coordinates vector by setting all points to zero, and then
% rebuilding the inner valus.
z = zeros(2*Nn^2,1);
for i = 1:length(uh_inner)
    z(inner_nodes(i)) = uh_inner(i);
end
 
% Plot solution
close all
for i=1:Nn^2
    zz(i)=z(2*i-1);
end
figure

trimesh(tri, p(:,1), p(:,2), zz);

figure

for i=1:Nn^2
    zz(i)=z(2*i);
end
trimesh(tri, p(:,1), p(:,2), zz);

% Plot error
err = zeros(Nn^2, 1);
zzz=zeros(Nn^2,1);
for i = 1:Nn
    for j=1:Nn
        k = (j-1)*Nn+i;
        x=-1+2/(Nn-1)*(i-1);
        y=-1+2/(Nn-1)*(j-1);
        exact=(x^2-1)*(y^2-1);
        zzz(k)=exact;
        err(k)=zz(k)-exact;
    end
end
figure
trimesh(tri, p(:,1), p(:,2), zzz);
figure
plot(err)
legend('Error');
