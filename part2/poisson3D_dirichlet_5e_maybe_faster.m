function disp=poisson3D_dirichlet_5e_maybe_faster(p,tri,edge)
Nn=size(p,1);
% Poisson solver in 3D
% See
% http://www.math.ntnu.no/emner/TMA4220/2013h/notes/TMA4220_2007-Lecture_Note_4.pdf
% for details.

% Variables
%   - Nn    the number of nodes in the mesh
%   - f     a function from R^2 to R
total_time=tic;
precalc=tic;
E=1;
nu=0.3;
%f = @(x) -8*pi*cos(2*pi*(x(1)^2 + x(2)^2)) + 16*pi^2*(x(1)^2+x(2)^2)*sin(2*pi*(x(1)^2+x(2)^2));
%f=@(x) E/(1-nu^2)*[-2*x(2)^2-x(1)^2+nu*x(1)^2-2*nu*x(1)*x(2)-2*x(1)*x(2)+3-nu;...
%    -2*x(1)^2-x(2)^2+nu*x(2)^2-2*nu*x(2)*x(1)-2*x(1)*x(2)+3-nu];

%Nn=size(p,1)
f1=@(x) 0;
f2=@(x) -0.01;
f3=@(x) 0; %??????????????????????????????????????????????????????????????


% Ensure the getDisk() function is in path, and close exisiting figures
addpath('grids');

close all

% Generate a disk mesh

%   - p     nodal points. (x,y)-coordinates for point i given in row i.
%   - tri   elements. Index to the three corners of element i given in row i.
%   - edge  edge lines. Index list to the two corners of edge line i given in row i

%[p, tri, edge] = getCube(Nn);

%Nn=Nn^3;


%edge=edge(6,:);
% Nn=6;
% p=[0,0,0;1,0,0;0,1,0;0,0,1;1,1,1;1,1,0];
% tri=[1,2,3,4;2,3,4,5;2,3,5,6];
% edge=[1,2,4];

K = size(tri,1);
%find nonzero values in Ah

%neighbours:
nonz=zeros(16*K,2);
for k=1:K
    s=sort(tri(k,:));
    for i=1:4
        for j=1:4
            nonz((k-1)*16+(i-1)*4+j,1)=s(i);
            nonz((k-1)*16+(i-1)*4+j,2)=s(j);
        end
    end
end
%delete doubles
%nonz
nonz=unique(nonz,'rows');

nonzl=size(nonz,1); %length
%nonz

%for large 3x3 blocks
nonz_in_Ah=zeros(9*nonzl,2);

for i=1:nonzl
    nonz_in_Ah((i-1)*9+1,:)=3*(nonz(i,:)-1)+1;
    nonz_in_Ah((i-1)*9+2,:)=[3*(nonz(i,1)-1)+1,3*(nonz(i,2)-1)+2];
    nonz_in_Ah((i-1)*9+3,:)=[3*(nonz(i,1)-1)+1,3*(nonz(i,2)-1)+3];
    nonz_in_Ah((i-1)*9+4,:)=[3*(nonz(i,1)-1)+2,3*(nonz(i,2)-1)+1];
    nonz_in_Ah((i-1)*9+5,:)=[3*(nonz(i,1)-1)+2,3*(nonz(i,2)-1)+2];
    nonz_in_Ah((i-1)*9+6,:)=[3*(nonz(i,1)-1)+2,3*(nonz(i,2)-1)+3];
    nonz_in_Ah((i-1)*9+7,:)=[3*(nonz(i,1)-1)+3,3*(nonz(i,2)-1)+1];
    nonz_in_Ah((i-1)*9+8,:)=[3*(nonz(i,1)-1)+3,3*(nonz(i,2)-1)+2];
    nonz_in_Ah((i-1)*9+9,:)=[3*(nonz(i,1)-1)+3,3*(nonz(i,2)-1)+3];
end
%nonz_in_Ah
nonz_in_Ahl=size(nonz_in_Ah,2);


% Build stiffness matrix Ah and load vector Fh
% Look trough all elements, and add contributions to the nodes.
%   - K    the number of elements in the mesh

Ah = sparse(nonz_in_Ah(:,1),nonz_in_Ah(:,2),ones(size(nonz_in_Ahl,1),1)/100000,3*Nn,3*Nn);
%Ah = sparse(3*Nn,3*Nn);
Fh = zeros(3*Nn,1);
%Ah_init=Ah;

%full(Ah)
% Unit vectors. Used for constructing the basis function planes on an
% element.
e = eye(4);
%5d
precalc=toc(precalc);
t_loop=tic;
t_quadr=0;
t_crate_Ak=0;
t_crate_Ak1=0;
t_crate_Ak2=0;
for k = 1:K
    if mod(k,500)==0 || k==K
        fprintf('%d of %d\n',k,K);
    end
    % The corner numbers for this element
    trik = tri(k,:);
    
    % The corner positions for this element
    pk = [p(trik(1),:);
        p(trik(2),:);
        p(trik(3),:);
        p(trik(4),:)];
    
    % The Volume of this element with the usual determinant formula.
    volume = 1/6*abs(det([pk(2,:)-pk(1,:);
        pk(3,:)-pk(1,:);
        pk(4,:)-pk(1,:)]));
    
    
    % The matrix used to solve for the coefficients of the basis functions
    % on element k. (They are a plane)
    Mk = [[1;1;1;1] pk];
    %5d
    
    % Find all the basis functions on this element. The alpha'th column has
    % the coefficients for the plane for corner alpha.
    % Phi on the alpha'th corner of element k is given by the plane
    % Hk(alpha) = ckalpha(1) + ckalpha(2)*x + ckalpha(3)*y+ ckalpha(4)*z. x, y,z and Hk is known at
    % the corners, and thus we can solve for ck.
    ck = zeros(4,4);
    for alpha = 1:4
        
        ck(:,alpha) = (Mk\e(:,alpha));
        
    end
    %changed for 5d
    
    % Ak is the elemental 3x3 matrix for this element, as given at page 13
    % in Lecture note 4.
    
    % C matrix in 3D:
    % http://www.wolframalpha.com/input/?i=invert+matrix+{{1%2Fx%2C+-v%2Fx%2C+-v%2Fx%2C+0%2C+0%2C+0}%2C+{-v%2Fx%2C+1%2Fx%2C+-v%2Fx%2C+0%2C+0%2C+0}%2C{-v%2Fx%2C+-v%2Fx%2C+1%2Fx%2C+0%2C+0%2C+0}%2C+{0%2C0%2C0%2C2*%281%2Bv%29%2Fx%2C0%2C0}%2C{0%2C0%2C0%2C0%2C2*%281%2Bv%29%2Fx%2C0}%2C{0%2C0%2C0%2C0%2C0%2C2*%281%2Bv%29%2Fx}}
    
    % expression for A_ij inside integral:
    % http://www.wolframalpha.com/input/?i={a%2Fx%2C+b%2Fy%2C+c%2Fz%2C+a%2Fy%2Bb%2Fx%2C+a%2Fz%2Bc%2Fx%2C+b%2Fz%2Bc%2Fy}*{{-%281-v%29%2F%282*v^2%2Bv-1%29%2C-v%2F%282*v^2%2Bv-1%29%2C-v%2F%282*v^2%2Bv-1%29%2C0%2C0%2C0}%2C{0%2C0%2C0%2C0%2C0%2C0}%2C{0%2C0%2C0%2C0%2C0%2C0}%2C{0%2C0%2C0%2C0%2C0%2C0}%2C{0%2C0%2C0%2C0%2C0%2C0}%2C{0%2C0%2C0%2C0%2C0%2C0}}
    
    C=E*[-(1-nu)/(2*nu^2+nu-1) -nu/(2*nu^2+nu-1) -nu/(2*nu^2+nu-1) 0 0 0;
        -nu/(2*nu^2+nu-1) -(1-nu)/(2*nu^2+nu-1) -nu/(2*nu^2+nu-1) 0 0 0;
        -nu/(2*nu^2+nu-1) -nu/(2*nu^2+nu-1) -(1-nu)/(2*nu^2+nu-1) 0 0 0;
        0 0 0 1/(2*nu+2) 0 0;
        0 0 0 0 1/(2*nu+2) 0;
        0 0 0 0 0 1/(2*nu+2)];
    
%     %if the first componet of phi_i is not empty the vector
%     %[eps_xx,eps_yy,eps_yy,eps_xy,eps_xz,eps_yz]^T is:
%     eps1=@(alpha)[ck(2,alpha);0;0;ck(3,alpha);ck(4,alpha);0];
%     %if the second componet of phi_i is not empty the vector
%     %[eps_xx,eps_yy,eps_yy,eps_xy,eps_xz,eps_yz]^T is:
%     eps2=@(alpha)[0;ck(3,alpha);0;ck(2,alpha);0;ck(4,alpha)];
%     %if the third componet of phi_i is not empty the vector
%     %[eps_xx,eps_yy,eps_yy,eps_xy,eps_xz,eps_yz]^T is:
%     eps3=@(alpha)[0;0;ck(4,alpha);0;ck(2,alpha);ck(3,alpha)];
%     
    
    
%     Ak = @(alpha, beta) volume * [eps1(alpha)'*C*eps1(beta),eps1(alpha)'*C*eps2(beta),eps1(alpha)'*C*eps3(beta);...
%         eps2(alpha)'*C*eps1(beta),eps2(alpha)'*C*eps2(beta),eps2(alpha)'*C*eps3(beta);...
%         eps3(alpha)'*C*eps1(beta),eps3(alpha)'*C*eps2(beta),eps3(alpha)'*C*eps3(beta)];
%     %Ak(1,1)
    %maybe transposed
    
    %(ck(2,alpha,gamma)*ck(2,beta,delta) + ck(2,alpha,gamma)*ck(6,beta,delta)+...
    % ck(6,alpha,gamma)*ck(2,beta,delta) + ck(6,alpha,gamma)*ck(6,beta,delta)+...
    %(1-nu)/2*(ck(3,alpha,gamma)*ck(3,beta,delta) + ck(3,alpha,gamma)*ck(5,beta,delta)+...
    %ck(5,alpha,gamma)*ck(3,beta,delta) + ck(5,alpha,gamma)*ck(5,beta,delta)));
    
    % The procedure for constructing Ah works by iterating over all
    % elements, and for each element constructing all the 9 possible
    % integrals of the basis functions. We have combinations 11,22,33,12,13
    % etc. These integrals then contribute to the corner points of the
    % element.
    for alpha = 1:4
        i= trik(alpha);
        
        %[eps_xx,eps_yy,eps_yy,eps_xy,eps_xz,eps_yz]^T is:
        eps1alpha=[ck(2,alpha);0;0;ck(3,alpha);ck(4,alpha);0];
        %if the second componet of phi_i is not empty the vector
        %[eps_xx,eps_yy,eps_yy,eps_xy,eps_xz,eps_yz]^T is:
        eps2alpha=[0;ck(3,alpha);0;ck(2,alpha);0;ck(4,alpha)];
        %if the third componet of phi_i is not empty the vector
        %[eps_xx,eps_yy,eps_yy,eps_xy,eps_xz,eps_yz]^T is:
        eps3alpha=[0;0;ck(4,alpha);0;ck(2,alpha);ck(3,alpha)];
        
        
        %for gamma=1:2
        % First, construct the load vector component using quadrature to
        % evaluate the integral. Hk is the plane over this element, defined
        % so that Hk(p1) = 1, Hk(p2) = 0, Hk(p3) = 0.
        
        % Fkalpha = integral of f * Hkalpha over the element
        %Hk = @(x) [ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2)+
        %ck(4,alpha)*x(3),0,0;...
        %   0,ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2)+
        %   ck(4,alpha)*x(3),0;...
        %   0,0,ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2)+ ck(4,alpha)*x(3)];
        %g = @(x)  Hk(x)*f(x);
        g1=@(x) (ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2)+ ck(4,alpha)*x(3))*f1(x);
        g2=@(x) (ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2)+ ck(4,alpha)*x(3))*f2(x);
        g3=@(x) (ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2)+ ck(4,alpha)*x(3))*f3(x);
        t_quadrc=tic;
        %Fkalpha = [0,...
        %    0,...
        %    quadrature3D(pk(1,:)', pk(2,:)', pk(3,:)', pk(4,:)', 5, g3)];
        Fkalpha = [quadrature3D(pk(1,:)', pk(2,:)', pk(3,:)', pk(4,:)', 5, g1),...
            quadrature3D(pk(1,:)', pk(2,:)', pk(3,:)', pk(4,:)', 5, g2),...
            quadrature3D(pk(1,:)', pk(2,:)', pk(3,:)', pk(4,:)', 5, g3)];
        t_quadrc=toc(t_quadrc);
        t_quadr=t_quadr+t_quadrc;
        %Fkalpha = [quadrature2D(pk(1,:)', pk(2,:)', pk(3,:)', 4, g1),quadrature2D(pk(1,:)', pk(2,:)', pk(3,:)', 4, g2)];
        %Fkalpha_a = quadrature3D(pk(1,:)', pk(2,:)', pk(3,:)', pk(4,:)', 5, g_a);
        
        %             Hk = @(x) ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2);
        %             g = @(x) f(x) * Hk(x);
        %             Fkalpha = quadrature2D(pk(1,:)', pk(2,:)', pk(3,:)', 4, g);
        Fh(3*i-2) = Fh(3*i-2) + Fkalpha(1);
        Fh(3*i-1) = Fh(3*i-1) + Fkalpha(2);
        Fh(3*i) = Fh(3*i) + Fkalpha(3);
        %
        
        % The remaining part is for Ah
        
        
        for beta = 1:4
            %for delta=1:2
            j= trik(beta);
            
            %if the first componet of phi_i is not empty the vector
            %[eps_xx,eps_yy,eps_yy,eps_xy,eps_xz,eps_yz]^T is:
            eps1beta=[ck(2,beta);0;0;ck(3,beta);ck(4,beta);0];
            %if the second componet of phi_i is not empty the vector
            %[eps_xx,eps_yy,eps_yy,eps_xy,eps_xz,eps_yz]^T is:
            eps2beta=[0;ck(3,beta);0;ck(2,beta);0;ck(4,beta)];
            %if the third componet of phi_i is not empty the vector
            %[eps_xx,eps_yy,eps_yy,eps_xy,eps_xz,eps_yz]^T is:
            eps3beta=[0;0;ck(4,beta);0;ck(2,beta);ck(3,beta)];
          
             Ak2 = volume * [eps1alpha'*C*eps1beta,eps1alpha'*C*eps2beta,eps1alpha'*C*eps3beta;...
        eps2alpha'*C*eps1beta,eps2alpha'*C*eps2beta,eps2alpha'*C*eps3beta;...
        eps3alpha'*C*eps1beta,eps3alpha'*C*eps2beta,eps3alpha'*C*eps3beta];
            
            tt=tic;
%            Ak11=ck(2,alpha)*C(1,1)*ck(2,beta)+ck(3,alpha)*C(4,4)*ck(3,beta)+ck(4,alpha)*C(5,5)*ck(4,beta);
%            Ak22=ck(3,alpha)*C(2,2)*ck(3,beta)+ck(2,alpha)*C(4,4)*ck(2,beta)+ck(4,alpha)*C(6,6)*ck(4,beta);
%            Ak33=ck(4,alpha)*C(3,3)*ck(4,beta)+ck(2,alpha)*C(5,5)*ck(2,beta)+ck(3,alpha)*C(6,6)*ck(3,beta);
%            Ak12=ck(2,alpha)*C(1,2)*ck(3,beta)+ck(4,alpha)*C(4,4)*ck(2,beta);
%            Ak13=ck(2,alpha)*C(1,3)*ck(4,beta)+ck(3,alpha)*C(5,5)*ck(2,beta);
%            Ak23=ck(3,alpha)*C(2,3)*ck(4,beta)+ck(4,alpha)*C(6,6)*ck(3,beta);
            
%            Ak = volume * [Ak11,Ak12,Ak13;Ak12,Ak22,Ak23;Ak13,Ak23,Ak33];
            
%            if abs(max(Ak-Ak2))>1e-6
%                'not uequal'
%            end
            
            te=toc(tt);
            t_crate_Ak1=t_crate_Ak1+te;
             
            tt=tic;
            Ah(3*i-2:3*i,3*j-2:3*j) = Ah(3*i-2:3*i,3*j-2:3*j) + Ak2;
            %end
            te=toc(tt);
            t_crate_Ak=t_crate_Ak+te;
        end
        
        %end
        
        
    end
end

t_loop=toc(t_loop);
% The system is now singular, and cannot be solved. This is because we have
% too many basis functions. To fix this, we will remove all boundary
% points, because they are zero anyways.

% Get the inner nodes, by subtracting the set of edge nodes from the set of
% all nodes.


t_select_inner_nodes=tic;
all_nodes = 1:Nn;
boundary_nodes = unique(edge);


inner_nodes_single = setdiff(all_nodes, boundary_nodes);
si=size(inner_nodes_single,2);
inner_nodes = zeros(1,3*si);

for i = 1:si
    inner_nodes(3*i-2)=3*inner_nodes_single(i)-2;
    inner_nodes(3*i-1)=3*inner_nodes_single(i)-1;
    inner_nodes(3*i)=3*inner_nodes_single(i);
end
%full((Ah~=0)-Ah_init)
% Select only the inner parts of everything
Ah_inner = Ah(inner_nodes, inner_nodes);
%full(Ah_inner)
Fh_inner = Fh(inner_nodes);
%tri_inner = tri(inner_nodes,:);
% Solve the inner part
t_select_inner_nodes=toc(t_select_inner_nodes);
t_solve_sys=tic;
uh_inner = Ah_inner \ Fh_inner;
t_solve_sys=toc(t_solve_sys);
t_plot=tic;
% Build z-coordinates vector by setting all points to zero, and then
% rebuilding the inner valus.
z = zeros(3*Nn,1);
for i = 1:length(uh_inner)
    z(inner_nodes(i)) = uh_inner(i);
end

% Plot solution
%close all
figure
tetramesh(tri,p,'FaceColor','red','EdgeColor','black');
camlight
lighting gouraud
xlabel('x')
ylabel('y')
zlabel('z')


%reshape(z,3,Nn)'
%p+reshape(z,3,Nn)'
figure
tetramesh(tri,p+reshape(z,3,Nn)','FaceColor','red','EdgeColor','black');
camlight
lighting gouraud

disp=reshape(z,3,Nn)';


t_plot=toc(t_plot);
total_time=toc(total_time);

total_time
precalc
t_loop
t_quadr
t_crate_Ak
t_crate_Ak1
t_crate_Ak2
t_select_inner_nodes
t_solve_sys
t_plot


