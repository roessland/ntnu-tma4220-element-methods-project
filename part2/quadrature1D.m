function [ I ] = quadrature1D( a,b, Nq, g )
%QUADRATURE2D Integrates a function from a to b using quadrature

%
%   I \in R          value of the integral
%   a \in R          first limit
%   b \in R          second limit
%   Nq \in [1, 4]    number of integration points
%   g: R \to R       function pointer

%%
% Gaussian quadrature in one dimensions takes the form
%
% $$ I_1 = \int_{-1}^1 g(z) dz \approx \sum_{q=1}^{N_q} \rho_q g(z_q), $$
%
% where z is given by x(z) = z*(b-a)/2 + (a+b)/2. Since the range has
% increased from [-1, 1] to [a, b] we need to multiply this integral:
%
% $$ I_2 = \int_a^b g(x) dx = \frac{b-a}2 I_1 $$ 



% Set quadrature parameters
%
% wq = weights
% zq = quadrature points on [-1, 1]
%
if Nq == 1
    wq = [2];
    zq = [0];
    
elseif Nq == 2
    wq = [1
          1];
      
    zq = [-sqrt(1/3)
          sqrt(1/3)];
      
elseif Nq == 3
    wq = [5/9
          8/9
          5/9];
      
    zq = [-sqrt(3/5) 
          0
          sqrt(3/5)];
      
elseif Nq == 4
    wq = [(18 - sqrt(30)) / 36
          (18 + sqrt(30)) / 36
          (18 + sqrt(30)) / 36
          (18 - sqrt(30)) / 36];
      
    zq = [-sqrt((3 + 2 * sqrt(6/5)) / 7)
          -sqrt((3 - 2 * sqrt(6/5)) / 7)
           sqrt((3 - 2 * sqrt(6/5)) / 7)
           sqrt((3 + 2 * sqrt(6/5)) / 7)];
else
    error('Only the 1, 2, 3 and 4 point rules are supported');
end

% Transform quadrature points on [-1, 1] to points on [a, b]
xq = zeros(length(a), length(zq));
for i = 1:length(zq)
    xq(:,i) = a + (zq(i)+1)/2 .* (b-a); 
end

% Compute quadrature
I = 0;
for n = 1:Nq
    I = I + wq(n) * g(xq(:,n));
end

% We need to multiply by the length of the interval. To support coordinates
% in one than more dimension, we take the euclidean norm of the vector
% between a and b.
I = norm((b-a))/2 * I;


end
