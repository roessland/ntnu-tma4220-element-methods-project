% NOTES:
% 1. Only works for Minecraft geometry!!! (because volume is constant)
% 2. Only works for forces in the y-direction!!!


Nn=size(p,1);
% Poisson solver in 3D
% See
% http://www.math.ntnu.no/emner/TMA4220/2013h/notes/TMA4220_2007-Lecture_Note_4.pdf
% for details.

% Variables
%   - Nn    the number of nodes in the mesh
%   - f     a function from R^2 to R
K = size(tri,1);
mat=zeros(K,1);
if size(tri,2)>4
    mat=tri(:,5);
    tri=tri(:,1:4);
end
total_time=tic;
precalc=tic;
E=1;
nu=0.3;



f1=@(x) 0;
f2=@(x) -0.0005; %minecraft models: y axis is vertical
f3=@(x) -0.00; 


addpath('grids');

close all




%find nonzero values in Ah

%neighbours:

nonz=zeros(16*K,2);
for k=1:K
    s=sort(tri(k,:));
    
    for i=1:4
        for j=1:4
            nonz((k-1)*16+(i-1)*4+j,1)=s(i);
            nonz((k-1)*16+(i-1)*4+j,2)=s(j);
        end
    end
end
%delete doubles
%nonz
nonz=unique(nonz,'rows');

nonzl=size(nonz,1); %length
%nonz

%for large 3x3 blocks
nonz_in_Ah=zeros(9*nonzl,2);

for i=1:nonzl
    nonz_in_Ah((i-1)*9+1,:)=3*(nonz(i,:)-1)+1;
    nonz_in_Ah((i-1)*9+2,:)=[3*(nonz(i,1)-1)+1,3*(nonz(i,2)-1)+2];
    nonz_in_Ah((i-1)*9+3,:)=[3*(nonz(i,1)-1)+1,3*(nonz(i,2)-1)+3];
    nonz_in_Ah((i-1)*9+4,:)=[3*(nonz(i,1)-1)+2,3*(nonz(i,2)-1)+1];
    nonz_in_Ah((i-1)*9+5,:)=[3*(nonz(i,1)-1)+2,3*(nonz(i,2)-1)+2];
    nonz_in_Ah((i-1)*9+6,:)=[3*(nonz(i,1)-1)+2,3*(nonz(i,2)-1)+3];
    nonz_in_Ah((i-1)*9+7,:)=[3*(nonz(i,1)-1)+3,3*(nonz(i,2)-1)+1];
    nonz_in_Ah((i-1)*9+8,:)=[3*(nonz(i,1)-1)+3,3*(nonz(i,2)-1)+2];
    nonz_in_Ah((i-1)*9+9,:)=[3*(nonz(i,1)-1)+3,3*(nonz(i,2)-1)+3];
end
%nonz_in_Ah
nonz_in_Ahl=size(nonz_in_Ah,2);


% Build stiffness matrix Ah and load vector Fh
% Look trough all elements, and add contributions to the nodes.
%   - K    the number of elements in the mesh

Ah = sparse(nonz_in_Ah(:,1),nonz_in_Ah(:,2),ones(size(nonz_in_Ahl,1),1)/1e100,3*Nn,3*Nn);
%Ah = sparse(3*Nn,3*Nn);
Fh = zeros(3*Nn,1);
%Ah_init=Ah;
Ck= zeros(3,4,K);
C=E*[-(1-nu)/(2*nu^2+nu-1) -nu/(2*nu^2+nu-1) -nu/(2*nu^2+nu-1) 0 0 0;
    -nu/(2*nu^2+nu-1) -(1-nu)/(2*nu^2+nu-1) -nu/(2*nu^2+nu-1) 0 0 0;
    -nu/(2*nu^2+nu-1) -nu/(2*nu^2+nu-1) -(1-nu)/(2*nu^2+nu-1) 0 0 0;
    0 0 0 1/(2*nu+2) 0 0;
    0 0 0 0 1/(2*nu+2) 0;
    0 0 0 0 0 1/(2*nu+2)];

%full(Ah)
% Unit vectors. Used for constructing the basis function planes on an
% element.
e = eye(4);
%5d
precalc=toc(precalc);
t_loop=tic;
t_quadr=0;
t_crate_Ak=0;
t_crate_Ak1=0;
t_crate_Ak2=0;

volume = 0.020833333333333; % for minecraft blocks only...
for k = 1:K
    if mod(k,500)==0 || k==K
        fprintf('%d of %d\n',k,K);
    end
    % The corner numbers for this element
    trik = tri(k,:);
    
    % The corner positions for this element
    pk = [p(trik(1),:);
        p(trik(2),:);
        p(trik(3),:);
        p(trik(4),:)];
    
    ck = zeros(4,4);
    for alpha = 1:4
        
        ck(:,alpha) = ([[1;1;1;1] pk]\e(:,alpha));
        
    end
    Ck(:,:,k)=ck(2:4,:);
    %changed for 5d
    
    % Ak is the elemental 3x3 matrix for this element, as given at page 13
    % in Lecture note 4.
    
    
    
   
    
    
    % The procedure for constructing Ah works by iterating over all
    % elements, and for each element constructing all the 9 possible
    % integrals of the basis functions. We have combinations 11,22,33,12,13
    % etc. These integrals then contribute to the corner points of the
    % element.
    for alpha = 1:4
        i= trik(alpha);
        
        
        

        % First, construct the load vector component using quadrature to
        % evaluate the integral. Hk is the plane over this element, defined
        % so that Hk(p1) = 1, Hk(p2) = 0, Hk(p3) = 0.
        
         
        %material depending gravity force
        if mat(k)==43 %counterweight
            matconst=8;
        elseif mat(k)==173 %rope
            matconst=0.02;
        elseif mat(k)==35 %steal
            matconst=1;    
        elseif mat(k)==1 %ground
            matconst=1; 
        elseif mat(k)==57 %container
            matconst=1;     
        else
            matconst=1;
        end
        
        ckA = [ck(1,1) ck(1,2) ck(1,3) ck(1,4);
               ck(2,1) ck(2,2) ck(2,3) ck(2,4);
               ck(3,1) ck(3,2) ck(3,3) ck(3,4);
               ck(4,1) ck(4,2) ck(4,3) ck(4,4)];
           
        g2=@(x) (ckA(1,alpha) + ckA(2,alpha)*x(1) + ckA(3,alpha)*x(2)+ ckA(4,alpha)*x(3))*f2(x)*matconst;
        
        
        Fh(3*i-1) = Fh(3*i-1) + g2((pk(1,:) + pk(2,:) + pk(3,:) + pk(4,:))/4)*volume;
        
        for beta = 1:4
         
            j= trik(beta);
            
            
            tt=tic;
            
            Ak = volume * [ckA(2,alpha)*C(1,1)*ckA(2,beta)+ckA(3,alpha)*C(4,4)*ckA(3,beta)+ckA(4,alpha)*C(5,5)*ckA(4,beta),...
                ckA(2,alpha)*C(1,2)*ckA(3,beta)+ckA(3,alpha)*C(4,4)*ckA(2,beta),...
                ckA(2,alpha)*C(1,3)*ckA(4,beta)+ckA(4,alpha)*C(5,5)*ckA(2,beta);
                ckA(3,alpha)*C(2,1)*ckA(2,beta)+ckA(2,alpha)*C(4,4)*ckA(3,beta),...
                ckA(3,alpha)*C(2,2)*ckA(3,beta)+ckA(2,alpha)*C(4,4)*ckA(2,beta)+ckA(4,alpha)*C(6,6)*ckA(4,beta),...
                ckA(3,alpha)*C(2,3)*ckA(4,beta)+ckA(4,alpha)*C(6,6)*ckA(3,beta);
                ckA(4,alpha)*C(3,1)*ckA(2,beta)+ckA(2,alpha)*C(5,5)*ckA(4,beta),...
                ckA(4,alpha)*C(3,2)*ckA(3,beta)+ckA(3,alpha)*C(6,6)*ckA(4,beta),...
                ckA(4,alpha)*C(3,3)*ckA(4,beta)+ckA(2,alpha)*C(5,5)*ckA(2,beta)+ckA(3,alpha)*C(6,6)*ckA(3,beta)];

            
            te=toc(tt);
            t_crate_Ak1=t_crate_Ak1+te;
            
            tt=tic;
            Ah(3*i-2:3*i,3*j-2:3*j) = Ah(3*i-2:3*i,3*j-2:3*j) + Ak;
         
            te=toc(tt);
            t_crate_Ak=t_crate_Ak+te;
        end
        
        %end
        
        
    end
end

t_loop=toc(t_loop);
% The system is now singular, and cannot be solved. This is because we have
% too many basis functions. To fix this, we will remove all boundary
% points, because they are zero anyways.

% Get the inner nodes, by subtracting the set of edge nodes from the set of
% all nodes.


t_select_inner_nodes=tic;
all_nodes = 1:Nn;
boundary_nodes = unique(edge);


inner_nodes_single = setdiff(all_nodes, boundary_nodes);
si=size(inner_nodes_single,2);
inner_nodes = zeros(1,3*si);

for i = 1:si
    inner_nodes(3*i-2)=3*inner_nodes_single(i)-2;
    inner_nodes(3*i-1)=3*inner_nodes_single(i)-1;
    inner_nodes(3*i)=3*inner_nodes_single(i);
end
%full((Ah~=0)-Ah_init)
% Select only the inner parts of everything
Ah_inner = Ah(inner_nodes, inner_nodes);
%full(Ah_inner)
Fh_inner = Fh(inner_nodes);
%tri_inner = tri(inner_nodes,:);
% Solve the inner part
t_select_inner_nodes=toc(t_select_inner_nodes);
t_solve_sys=tic;
uh_inner = Ah_inner \ Fh_inner;
t_solve_sys=toc(t_solve_sys);
t_plot=tic;
% Build z-coordinates vector by setting all points to zero, and then
% rebuilding the inner valus.
z = zeros(3*Nn,1);
for i = 1:length(uh_inner)
    z(inner_nodes(i)) = uh_inner(i);
end
disp=reshape(z,3,Nn)';
%claculate stress
sigma=zeros(6,Nn);
tetra_counter=zeros(1,Nn);
for k=1:K
    trik = tri(k,:);
    lin_fu=zeros(6,1);
    for alpha=1:4
        i= trik(alpha);
        lin_fu=lin_fu+[disp(i,1)*Ck(1,alpha,k);disp(i,2)*Ck(2,alpha,k);disp(i,3)*Ck(3,alpha,k);...
            disp(i,1)*Ck(2,alpha,k)+disp(i,2)*Ck(1,alpha,k);disp(i,1)*Ck(3,alpha,k)+disp(i,3)*Ck(1,alpha,k);disp(i,3)*Ck(2,alpha,k)+disp(i,2)*Ck(3,alpha,k)];
    end
    for alpha=1:4
        i= trik(alpha);
        sigma(:,i)=sigma(:,i)+lin_fu;
        tetra_counter(i)=tetra_counter(i)+1;
    end   
end
for i=1:Nn
    sigma(:,i)=sigma(:,i)/tetra_counter(i);
end
sigma=C*sigma;
stress=zeros(Nn,1);
for i=1:Nn
    stress(i)= sqrt(1/2*((sigma(1,i)-sigma(2,i))^2+(sigma(2,i)-sigma(3,i))^2+(sigma(3,i)-sigma(1,i))^2+6*(sigma(4,i)^2+sigma(5,i)^2+sigma(6,i)^2)));
end




disp=reshape(z,3,Nn)';

t_plot=toc(t_plot);
total_time=toc(total_time);


precalc
t_loop
t_quadr
t_crate_Ak
t_crate_Ak1
t_crate_Ak2
t_select_inner_nodes
t_solve_sys
t_plot
total_time
tetramesh(tetr, pts + 200*disp);
view(90, 0)