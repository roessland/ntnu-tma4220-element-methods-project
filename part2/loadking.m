function [p tri edge]=loadking

p=importdata('../models/king/coarse_nodes.m');
tri=importdata('../models/king/coarse_element.m');
edge=importdata('../models/king/coarse_bndr2.m');
