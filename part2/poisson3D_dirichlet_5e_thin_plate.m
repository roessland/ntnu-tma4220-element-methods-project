
% Solve linear elasticity equation for a thin plate, using the reference
% solution given for 2D. The solutions is very similar to the analytic
% solution, and proves that our solution is correct, at least for zero
% forces in the z-direction.

% Variables
%   - side_dimension    number of nodes in each side of the plate. 20x20
%                       runs fast, while 50x50 uses a few minutes.
%   - E - material constant
%   - nu - material constant
tic;
side_dimension = 10;
E=1;
nu=0.3;

% Forces on each element, in the x, y and z directions
f1=@(x) E/(1-nu^2)*(-2*x(2)^2-x(1)^2+nu*x(1)^2-2*nu*x(1)*x(2)-2*x(1)*x(2)+3-nu);
f2=@(x) E/(1-nu^2)*(-2*x(1)^2-x(2)^2+nu*x(2)^2-2*nu*x(1)*x(2)-2*x(1)*x(2)+3-nu);
f3=@(x) 0;


% Ensure the getThinPlate function is in path, and close exisiting figures
addpath('grids');

close all

% Generate a disk mesh

%   - p     nodal points. (x,y)-coordinates for point i given in row i.
%   - tri   tetrahedral elements. Index to the three corners of element i given in row i.
%   - edge  edge lines. Index list to the two corners of edge line i given in row i

[p, tri, edge] = getThinPlate(side_dimension);

% The number of nodes
Nn=length(p);


%edge=edge(6,:);
% Nn=6;
% p=[0,0,0;1,0,0;0,1,0;0,0,1;1,1,1;1,1,0];
% tri=[1,2,3,4;2,3,4,5;2,3,5,6];
% edge=[1,2,4];

K = size(tri,1);
Ah = sparse(3*Nn,3*Nn);
Fh = zeros(3*Nn,1);

% Unit vectors. Used for constructing the basis function planes on an
% element.
e = eye(4);
tot=0;
for k = 1:K
    % The corner numbers for this element
    trik = tri(k,:);
    
    % The corner positions for this element
    pk = [p(trik(1),:);
          p(trik(2),:);
          p(trik(3),:);
          p(trik(4),:)];
      
    % The Volume of this element with the usual determinant formula.
    volume = 1/6*abs(det([pk(2,:)-pk(1,:);
                        pk(3,:)-pk(1,:);
                        pk(4,:)-pk(1,:)]));
                    
      
    % The matrix used to solve for the coefficients of the basis functions
    % on element k. (They are a plane)
    Mk = [[1;1;1;1] pk];
        %5d
    
    % Find all the basis functions on this element. The alpha'th column has
    % the coefficients for the plane for corner alpha.
    % Phi on the alpha'th corner of element k is given by the plane
    % Hk(alpha) = ckalpha(1) + ckalpha(2)*x + ckalpha(3)*y+ ckalpha(4)*z. x, y,z and Hk is known at
    % the corners, and thus we can solve for ck.
    ck = zeros(4,4);
    for alpha = 1:4
        
            ck(:,alpha) = (Mk\e(:,alpha));
        
    end
        %changed for 5d
    
    % Ak is the elemental 3x3 matrix for this element, as given at page 13
    % in Lecture note 4.

    % C matrix in 3D:
    % http://www.wolframalpha.com/input/?i=invert+matrix+{{1%2Fx%2C+-v%2Fx%2C+-v%2Fx%2C+0%2C+0%2C+0}%2C+{-v%2Fx%2C+1%2Fx%2C+-v%2Fx%2C+0%2C+0%2C+0}%2C{-v%2Fx%2C+-v%2Fx%2C+1%2Fx%2C+0%2C+0%2C+0}%2C+{0%2C0%2C0%2C2*%281%2Bv%29%2Fx%2C0%2C0}%2C{0%2C0%2C0%2C0%2C2*%281%2Bv%29%2Fx%2C0}%2C{0%2C0%2C0%2C0%2C0%2C2*%281%2Bv%29%2Fx}}
    
    % expression for A_ij inside integral:
    % http://www.wolframalpha.com/input/?i={a%2Fx%2C+b%2Fy%2C+c%2Fz%2C+a%2Fy%2Bb%2Fx%2C+a%2Fz%2Bc%2Fx%2C+b%2Fz%2Bc%2Fy}*{{-%281-v%29%2F%282*v^2%2Bv-1%29%2C-v%2F%282*v^2%2Bv-1%29%2C-v%2F%282*v^2%2Bv-1%29%2C0%2C0%2C0}%2C{0%2C0%2C0%2C0%2C0%2C0}%2C{0%2C0%2C0%2C0%2C0%2C0}%2C{0%2C0%2C0%2C0%2C0%2C0}%2C{0%2C0%2C0%2C0%2C0%2C0}%2C{0%2C0%2C0%2C0%2C0%2C0}}
    
    C=E*[-(1-nu)/(2*nu^2+nu-1) -nu/(2*nu^2+nu-1) -nu/(2*nu^2+nu-1) 0 0 0;
       -nu/(2*nu^2+nu-1) -(1-nu)/(2*nu^2+nu-1) -nu/(2*nu^2+nu-1) 0 0 0;
       -nu/(2*nu^2+nu-1) -nu/(2*nu^2+nu-1) -(1-nu)/(2*nu^2+nu-1) 0 0 0;
       0 0 0 1/(2*nu+2) 0 0;
       0 0 0 0 1/(2*nu+2) 0;
       0 0 0 0 0 1/(2*nu+2)];
   
  %if the first componet of phi_i is not empty the vector
   %[eps_xx,eps_yy,eps_yy,eps_xy,eps_xz,eps_yz]^T is:
   eps1=@(alpha)[ck(2,alpha);0;0;ck(3,alpha);ck(4,alpha);0];
   %if the second componet of phi_i is not empty the vector
   %[eps_xx,eps_yy,eps_yy,eps_xy,eps_xz,eps_yz]^T is:
   eps2=@(alpha)[0;ck(3,alpha);0;ck(2,alpha);0;ck(4,alpha)];
   %if the third componet of phi_i is not empty the vector
   %[eps_xx,eps_yy,eps_yy,eps_xy,eps_xz,eps_yz]^T is:
   eps3=@(alpha)[0;0;ck(4,alpha);0;ck(2,alpha);ck(3,alpha)];
   
   
   
    Ak = @(alpha, beta) volume * [eps1(alpha)'*C*eps1(beta),eps1(alpha)'*C*eps2(beta),eps1(alpha)'*C*eps3(beta);...
        eps2(alpha)'*C*eps1(beta),eps2(alpha)'*C*eps2(beta),eps2(alpha)'*C*eps3(beta);...
        eps3(alpha)'*C*eps1(beta),eps3(alpha)'*C*eps2(beta),eps3(alpha)'*C*eps3(beta)];

    % The procedure for constructing Ah works by iterating over all
    % elements, and for each element constructing all the 9 possible
    % integrals of the basis functions. We have combinations 11,22,33,12,13
    % etc. These integrals then contribute to the corner points of the
    % element.
    for alpha = 1:4
        i= trik(alpha);
        % First, construct the load vector component using quadrature to
        % evaluate the integral. Hk is the plane over this element, defined
        % so that Hk(p1) = 1, Hk(p2) = 0, Hk(p3) = 0.

        g1=@(x) (ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2)+ ck(4,alpha)*x(3))*f1(x);
        g2=@(x) (ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2)+ ck(4,alpha)*x(3))*f2(x);
        g3=@(x) (ck(1,alpha) + ck(2,alpha)*x(1) + ck(3,alpha)*x(2)+ ck(4,alpha)*x(3))*f3(x);
        Fkalpha = [quadrature3D(pk(1,:)', pk(2,:)', pk(3,:)', pk(4,:)', 5, g1),...
                   quadrature3D(pk(1,:)', pk(2,:)', pk(3,:)', pk(4,:)', 5, g2),...
                   quadrature3D(pk(1,:)', pk(2,:)', pk(3,:)', pk(4,:)', 5, g3)];

         Fh(3*i-2) = Fh(3*i-2) + Fkalpha(1);
         Fh(3*i-1) = Fh(3*i-1) + Fkalpha(2);
         Fh(3*i) = Fh(3*i) + Fkalpha(3);

        % The remaining part is for Ah

        for beta = 1:4
            j= trik(beta);
            tt=tic;
            Ah(3*i-2:3*i,3*j-2:3*j) = Ah(3*i-2:3*i,3*j-2:3*j) + Ak(alpha, beta);
            te=toc(tt);
            tot=tot+te;
        end
    end
end
tot

% The system is now singular, and cannot be solved. This is because we have
% too many basis functions. To fix this, we will remove all boundary
% points, because they are zero anyways.

% Get the inner nodes, by subtracting the set of edge nodes from the set of
% all nodes.
toc

tic
all_nodes = 1:Nn;
boundary_nodes = unique(edge);


inner_nodes_single = setdiff(all_nodes, boundary_nodes);
si=size(inner_nodes_single,2);
inner_nodes = zeros(1,3*si);

for i = 1:si
    inner_nodes(3*i-2)=3*inner_nodes_single(i)-2;
    inner_nodes(3*i-1)=3*inner_nodes_single(i)-1;
    inner_nodes(3*i)=3*inner_nodes_single(i);
end

% Select only the inner parts of everything
Ah_inner = Ah(inner_nodes, inner_nodes);
Fh_inner = Fh(inner_nodes);

% Solve the inner part
toc
tic 
uh_inner = Ah_inner \ Fh_inner;
toc
tic

% Build z-coordinates vector by setting all points to zero, and then
% rebuilding the inner valus.
z = zeros(3*Nn,1);
for i = 1:length(uh_inner)
    z(inner_nodes(i)) = uh_inner(i);
end
 
% Plot mesh
figure
tetramesh(tri,p,'FaceColor','red','EdgeColor','black');
camlight 
lighting gouraud
xlabel('x')
ylabel('y')
zlabel('z')

% Plot deformed mesh
displ = reshape(z, 3, Nn)';
figure
tetramesh(tri,p+displ,'FaceColor','red','EdgeColor','black');
camlight 
lighting gouraud
xlabel('x')
ylabel('y')
zlabel('z')

% Compare with analytical solution
u = @(x, y, z) [ (x.^2 - 1).*(y.^2 - 1)  (x.^2 - 1).*(y.^2 - 1)  zeros(length(x), 1)];

% Plot analytic deformed mesh
figure
displ_analytic = u(p(:,1), p(:,2), p(:,3));
tetramesh(tri, p + displ_analytic, 'FaceColor','red','EdgeColor','black');
camlight
lighting gouraud
xlabel('x')
ylabel('y')
zlabel('z')

% Plot error
figure
err = displ-displ_analytic;
quiver(p(:,1), p(:,2), err(:,1), err(:,2));

toc

