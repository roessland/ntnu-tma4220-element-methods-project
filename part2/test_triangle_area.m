function test_suite = test_triangle_area
    initTestSuite
end


% Test that the area is zero if the triangle is degenerate,
% that is, if three points are on teh same line
function testZeroArea2DTriangle
    p1 = [1 1]'; p2 = [2 2]'; p3 = [3 3]';
    
    actual_area = 0;
    
    assertEqual(triangle_area(p1,p2,p3), actual_area);
end

% Test the area of a 3D triangle in a single plane
function testSimple3DTriangle
    p1 = [1 0 0]'; p2 = [0 0 1]'; p3 = [0 0 0]';
    
    actual_area = 1/2;
    
    assertEqual(actual_area, triangle_area(p1,p2,p3));
end