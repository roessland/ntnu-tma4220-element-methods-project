clear all
% Poisson solver in 3D
% See
% http://www.math.ntnu.no/emner/TMA4220/2013h/notes/TMA4220_2007-Lecture_Note_4.pdf
% for details.

% Variables
%   - Nn    the number of nodes in the mesh
%   - f     a function from R^3 to R for the inner condition.
%   - g     a function from R^3 to R for the boundary condition.
Nn = 200;

% The f given for this task, and the analytic solution
f = @(x) -12*pi*cos(2*pi*norm(x)^2) + 16*pi^2*norm(x)^2*sin(2*pi*norm(x)^2);
u_analytic = @(x) sin(pi*norm(x)^2);


% Ensure the getDisk() function is in path
addpath('grids');


% Plot our solution
F = @(x,y,z) u_analytic([x y z]);

% M is the amount of grid points in each direction of the axes,
% where we will sample the interpolant F.
M = 41;

% Create axes. Cut x in half so that we can see inside the sphere.
x = linspace(0, 1, M);
y = linspace(-1, 1, M);
z = linspace(-1, 1, M);

% Construct a three dimensional matrix v, which will hold the values of the
% analytic function at the grid points defined by the axes x, y and z. Is
% there a better way to do this without looping?
v = zeros(M, M, M);
for i = 1:M
    for j = 1:M
        for k = 1:M
            v(i,j,k) = F(x(i), y(j), z(k));
        end
    end
end

% How many isosurfaces to draw
num_isosurfaces = 10;

% Draw isosurfaces for iso = -1 to 1
iso = linspace(-1, 1, num_isosurfaces);

% Define a colormap to use. The "hot" colormap has 64 rows, of colors,
% where each row contains R, G and B values. Define one color for each
% isosurface.
cmap = colormap(hot);
num_colors = size(cmap, 1);

% Get one color for each isosurface
color = cmap(round(linspace(1, num_colors, num_isosurfaces)),:);

for i = 1:num_isosurfaces
    pp = patch(isosurface(x, y, z, v, iso(i)));
    isonormals(x, y, z, v, pp)
    set(pp,'FaceColor', color(i,:),'EdgeColor','none');
    %daspect([1,1,1])
    %view(3); axis tight
    camlight 
    lighting gouraud
    hold on
end