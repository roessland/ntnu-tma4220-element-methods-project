runtests test_tetrahedron_volume
tic
for i = 1:25000
    P = rand(3, 4);
    tetrahedron_volume(P(:,1), P(:,2), P(:,3), P(:,4));
end
toc