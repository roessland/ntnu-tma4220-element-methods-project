function [ volume ] = tetrahedron_volume( p1, p2, p3, p4 )
%TETRAHEDRON_VOLUME Returns the volume between four points
% p1,p2,p3,p4 must be vertical XYZ vectors.

% Check that the vectors are vertical
assert(size(p1, 1) == 3);
assert(size(p2, 1) == 3);
assert(size(p3, 1) == 3);
assert(size(p4, 1) == 3);

%volume = 1/6 * abs( (p1-p4)' * cross(p2-p4, p3-p4)); % 2.5 times slower!
volume = 1/6 * abs(det([p2-p1 p3-p1 p4-p1]));

end