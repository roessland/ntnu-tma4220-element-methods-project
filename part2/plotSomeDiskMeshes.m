% Generates plots for problem 2d

addpath('grids');
close all

% Generate a disk mesh
%   - N     the number of nodes in the mesh
%   - p     nodal points. (x,y)-coordinates for point i given in row i.
%   - tri   elements. Index to the three corners of element i given in row i.
%   - edge  edge lines. Index list to the two corners of edge line i given in row i

N = 20;
[p tri edge] = getDisk(N);



% Plot the disk mesh.
% Note that trimesh takes x and y positions as separate vectors.
figure
trimesh(tri, p(:,1), p(:,2));


% Repeat for other mesh sizes
[p tri edge] = getDisk(100);
figure
trimesh(tri, p(:,1), p(:,2));

[p tri edge] = getDisk(1000);
figure
trimesh(tri, p(:,1), p(:,2));